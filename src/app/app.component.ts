import { Component } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { NavigationEnd, NavigationStart, ResolveStart, Router } from '@angular/router';
import { device } from 'src/environments/device';
import { GoogleAnalyticsService } from './shared/services/google-analytics.service';
import { MediaService } from './shared/services/media.service';

@Component({
  selector: 'mca-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mca';
  routing : boolean;
  get lg(): boolean{
    return (device.mobile && this.media.lg) || this.media.lg;
  }
  constructor(private media: MediaService, private router: Router, private ga: GoogleAnalyticsService){
    // for some reason without this subscription ssrObserverBreakpoint have no effect
    media.observer.asObservable().subscribe(observer => {})

    this.routing = false;
    router.events.subscribe((event) =>{
      if(event instanceof ResolveStart){
        this.routing = true;
      }else if(event instanceof NavigationEnd){
        this.ga.logPageView(event.url);
        this.routing = false;
      }

    })
  }
}
