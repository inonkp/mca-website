import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'mca-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit {
  rating: number;
  stars = [1, 2, 3, 4, 5];

  @Output() setRating = new EventEmitter<number>();

  private _userStars: number;
  @Input() set userStars(val: number) {
    this._userStars = val;
    this.rating = this._userStars;
  }
  get userStars(): number {
    return this._userStars;
  }

  @Input('disabled') isDisabled: boolean;

  private _resetRating: boolean;
  @Input() set resetRating(val: boolean) {
    this._resetRating = val;
    if (this._resetRating) {
      this.rating = 0;
    }
  }

  get resetRating(): boolean {
    return this._resetRating;
  }

  constructor() {
    this.rating = 0;
    this.isDisabled = false;
  }

  ngOnInit(): void {}

  public onClick(ratingValue, event): void {
    event.stopPropagation();
    this.rating = ratingValue;
    if (this.isDisabled) {
      this.rating = this.userStars;
    } else {
      this.setRating.emit(this.rating);
    }
  }
}
