import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { DataServiceError, EntityCollectionDataService, EntityDataService } from '@ngrx/data';
import { SupportRequest } from 'src/app/shared/entities/support-request';
import Catch from 'catch-finally-decorator';
import { McaSeoService } from 'src/app/shared/services/mca-seo.service';

@Component({
  selector: 'mca-contact-page',
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.scss']
})
export class ContactPageComponent implements OnInit {
  supportService: EntityCollectionDataService<SupportRequest>
  formGroup: UntypedFormGroup;
  loading: boolean;
  error: string;
  createdRequest: SupportRequest;
  constructor(fb: UntypedFormBuilder, entityDataServices: EntityDataService, private seo: McaSeoService) {
    seo.noindex();
    seo.setTitle('Contact Us');
    this.formGroup = fb.group({
      name: ["", Validators.required],
      email: ["", Validators.required],
      order_id: [""],
      subject: ["", Validators.required],
      body: ["", Validators.required]
    });
    this.supportService = entityDataServices.getService("SupportRequest");
    this.loading = false;
   }

  ngOnInit(): void {
  }

  @Catch(DataServiceError, (err: DataServiceError ,ctx: ContactPageComponent ) => {
      ctx.error = err.message;
      console.log(err)
    },
    (ctx: ContactPageComponent) =>{
      ctx.loading = false;
      ctx.formGroup.enable({emitEvent: false});
    })
  async submit(form: FormGroupDirective){
    this.error = null;
    this.createdRequest = null;
    this.loading = true;
    this.formGroup.disable({emitEvent: false})
    const request = this.formGroup.value;
    this.createdRequest = await this.supportService.add(request).toPromise();
    form.resetForm();
    this.formGroup.reset();
  }

}
