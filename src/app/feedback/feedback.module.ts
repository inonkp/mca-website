import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RatingComponent } from './rating/rating.component';
import { StarComponent } from './rating/star/star.component';
import { StarfillComponent } from './rating/starfill/starfill.component';
import { CommentsComponent } from './comments/comments.component';
import { SharedModule } from '../shared/shared.module';
import { CommentFormComponent } from './comment-form/comment-form.component';
import { UserCommentsComponent } from './user-comments/user-comments.component';
import { ContactPageComponent } from './contact-page/contact-page.component';


@NgModule({
  declarations: [
    RatingComponent,
    StarComponent,
    CommentsComponent,
    StarfillComponent,
    CommentFormComponent,
    UserCommentsComponent,
    ContactPageComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    CommentsComponent,
    CommentFormComponent,
    UserCommentsComponent
  ]
})
export class FeedbackModule { }
