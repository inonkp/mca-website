import {Component, Input, OnInit} from '@angular/core';
import {MCAComment} from '../../shared/entities/mca-comment';
import {EntityCollectionDataService, EntityDataService} from '@ngrx/data';
import {ProductsService} from '../../catalog/services/products.service';

@Component({
  selector: 'mca-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {
  allUserComments: MCAComment[] = [];
  commentService: EntityCollectionDataService<MCAComment>;

  private _productId: string;

  @Input() set productId(value: string) {
    this._productId = value;
    this.getAllComments(this._productId);
  }

  get productId(): string {
    return this._productId;
  }

  constructor(entityDataService: EntityDataService,
              private productsService: ProductsService) {
    this.commentService = entityDataService.getService('MCAComment');
  }

  ngOnInit(): void {}

  public async getAllComments(prodId: string) {

    this.allUserComments = await this.commentService
        .getWithQuery({
          'product_id': prodId
        }).toPromise();
  }
}
