import {Component, Input, OnInit} from '@angular/core';
import {MCAComment} from '../../shared/entities/mca-comment';
import {FullImageDialogComponent} from '../../shared/components/full-image-dialog/full-image-dialog.component';
import {take} from 'rxjs/operators';
import {MatDialog} from '@angular/material/dialog';
import { ImageResizerService } from 'src/app/shared/services/image-resizer.service';
import { EntityCollectionDataService, EntityDataService } from '@ngrx/data';
import { faComments } from "@fortawesome/free-solid-svg-icons/faComments";

@Component({
  selector: 'mca-user-comments',
  templateUrl: './user-comments.component.html',
  styleUrls: ['./user-comments.component.scss']
})
export class UserCommentsComponent implements OnInit {

  get faComments(){
    return faComments;
  }

  allUserComments: MCAComment[];
  @Input() productId: string;
  commentService: EntityCollectionDataService<MCAComment>;

  constructor(entityDataService: EntityDataService, private imageResizer: ImageResizerService) {
    this.commentService = entityDataService.getService('MCAComment');
    this.allUserComments = [];
  }

  ngOnInit(): void {
    this.init();
  }

  async init(){
    this.allUserComments = await this.commentService
        .getWithQuery({
          'product_id': this.productId
        }).toPromise();
  }

  public openCommentImgModal(img: any): void {
    this.imageResizer.openLargeImgModal(img.url);
  }

  getCommentIcon(img: string){
    return this.imageResizer.getResized(img,100,100);
  }
}
