import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import { AbstractControl, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { CommentStatus, MCAComment } from '../../shared/entities/mca-comment';
import { HttpUrlGenerator } from '@ngrx/data';
import { HttpClient } from '@angular/common/http';
import Catch from 'catch-finally-decorator';

@Component({
  selector: 'mca-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.scss']
})
export class CommentFormComponent implements OnInit {
  commentForm: UntypedFormGroup;
  resetRating: boolean;
  files: FileList;
  filesArray: File[];
  loading: boolean;
  commentSubmitted: boolean;
  @ViewChild('inputFile') multipleFiles: ElementRef;

  @Input() allUserComments: MCAComment[];
  @Input() productId: string;

  constructor(
    private httpUrlGenerator: HttpUrlGenerator,
    private http: HttpClient
  ) {
    this.commentSubmitted = false;
    this.loading = false;
   }

  ngOnInit(): void {
    this.generateCommentForm();
  }

  public generateCommentForm(): void {
    this.commentForm = new UntypedFormGroup({
      name: new UntypedFormControl(''),
      rating: new UntypedFormControl('', Validators.required),
      comment: new UntypedFormControl('', [Validators.required]),
      file: new UntypedFormControl('')
    });
  }

  public setRating(value): void {
    this.commentForm?.get('rating')?.setValue(value);
  }

  get formInfoField(): { [p: string]: AbstractControl } {
    return this.commentForm.controls;
  }

  public fileLoaded(event: any): void {
    this.updateFileList(event.currentTarget.files);
  }

  updateFileList(files: FileList){
    this.files = files;
    this.filesArray = files ? Array.from(this.files) : [];
  }

  public removeFile(index): void {
    const newFileList = Array.from(this.files);
    newFileList.splice(index, 1);

    const dT = new DataTransfer();
    for (const file of newFileList) { dT.items.add(file); }
    this.updateFileList(dT.files);
  }

  @Catch(Error,(err) => {}, (ctx: CommentFormComponent) => {ctx.loading = false;})
  async submit(): Promise<void> {
    this.loading = true;
    this.commentForm.disable()
    this.resetRating = false;
    this.commentSubmitted = false;

    const formData = new FormData();
    let fileNames = [];
    if (this.files) {
      for (let i = 0; i < this.files.length; i++) {
        fileNames.push(this.files.item(i).name);

      }
    }

    const paramComment = {
      stars: this.commentForm.get('rating').value,
      comment: this.commentForm.get('comment').value,
      product_id: this.productId,
      name: this.commentForm.get('name').value,
      fileNames: fileNames,
      status: CommentStatus.PENDING
    };

    // formData.append('data', JSON.stringify(paramComment));

    const commetData: any = await this.http.post(this.httpUrlGenerator.entityResource('MCAComment', '', true), paramComment).toPromise();
    let signedUrls: string[] = commetData.signedUrls ? commetData.signedUrls : [];
    for(let i=0; i< signedUrls.length; i++){
      // headers.append("Accept", "*/* asdfdsa")
      await this.http.put(signedUrls[i],this.files.item(i),{

      }).toPromise();

    }
    // this.allUserComments.push(commetData.comment as MCAComment);
    this.commentSubmitted = true;
    this.resetRating = true;
    this.commentForm.enable();
    this.commentForm.reset();
    this.multipleFiles.nativeElement.value = '';
    this.updateFileList(null);

  }
}
