import { TestBed } from '@angular/core/testing';

import { CheckoutResolverService } from './checkout-resolver.service';

describe('CheckoutResolverService', () => {
  let service: CheckoutResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CheckoutResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
