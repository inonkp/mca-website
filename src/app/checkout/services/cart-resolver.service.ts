import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { CartItem } from 'src/app/shared/entities/cart-item';
import { CartService } from './cart.service';

@Injectable({
  providedIn: 'root'
})
export class CartResolverService implements Resolve<CartItem[]> {

  constructor(private cartService: CartService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): CartItem[] | Observable<CartItem[]> | Promise<CartItem[]> {
    return this.cartService.getPendingCartItems();
  }
}
