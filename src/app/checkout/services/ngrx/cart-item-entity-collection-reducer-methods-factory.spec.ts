import { TestBed } from '@angular/core/testing';

import { CartItemEntityCollectionReducerMethodsFactoryService } from './cart-item-entity-collection-reducer-methods-factory.service';

describe('CartItemEntityCollectionReducerMethodsFactoryService', () => {
  let service: CartItemEntityCollectionReducerMethodsFactoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CartItemEntityCollectionReducerMethodsFactoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
