import { Injectable } from "@angular/core";
import { DefaultPersistenceResultHandler, EntityAction, EntityOp } from "@ngrx/data";
import { Action } from "@ngrx/store";

@Injectable()
export class CartPersistenceResultHandler extends DefaultPersistenceResultHandler {
    handleSuccess(originalAction: EntityAction): (data: any) => Action {
        const actionHandler = super.handleSuccess(originalAction);
        // return a factory to get a data handler to
        // parse data from DataService and save to action.payload
        return function(data: any) {
          const action = actionHandler.call(this, data);
          if (action && data && 
            originalAction.payload.entityName == "CartItem") {
            // save the data.foo to action.payload.foo
            if(data.items) {
                (action as any).payload.price = data.price;
                (action as any).payload.weight = data.weight;
                (action as any).payload.data = data.items;
            }
          }
          return action;
        };
      }
}
