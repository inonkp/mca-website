import { Injectable } from '@angular/core';
import { EntityCollectionReducerMethodMap, EntityCollectionReducerMethods, EntityDefinitionService } from '@ngrx/data';
import { CartItem } from 'src/app/shared/entities/cart-item';
import { CartItemEntityCollectionReducerMethods } from './cart-item-entity-collection-reducer-methods';

@Injectable()
export class CartItemEntityCollectionReducerMethodsFactory {

  constructor(private entityDefinitionService: EntityDefinitionService) {}
   /** Create the  {EntityCollectionReducerMethods} for the named entity type */
  create(entityName: string): EntityCollectionReducerMethodMap<CartItem> {
    
    const definition = this.entityDefinitionService.getDefinition<CartItem>(entityName);
    let methodsClass = new EntityCollectionReducerMethods(entityName, definition);
    if(entityName == "CartItem") {
      methodsClass = new CartItemEntityCollectionReducerMethods(entityName, definition);
    }
    return methodsClass.methods;
  }
}
