import { EntityAction, EntityCollection, EntityCollectionReducerMethods, EntityDefinition, UpdateResponseData } from "@ngrx/data";
import { CartItem } from "src/app/shared/entities/cart-item";

export class CartItemEntityCollectionReducerMethods 
    extends EntityCollectionReducerMethods<CartItem>  {
        constructor(public entityName: string, public definition: EntityDefinition<CartItem>) {
            super(entityName, definition);
          }
          
           protected queryManySuccess(
            collection: EntityCollection<CartItem>,
            action: EntityAction<CartItem[]>
          ): EntityCollection<CartItem> {
            const ec = super.queryManySuccess(collection, action);
            if (action.payload,this.entityName == "CartItem") {
              // save the foo property from action.payload to entityCollection instance
              (ec as any).price = (action.payload as any).price;
              (ec as any).weight = (action.payload as any).weight;
            }
            return ec;
          }

}
