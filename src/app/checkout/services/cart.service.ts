import { Inject, Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { CartItem, CartItemStatus } from '../../shared/entities/cart-item';
import { DataServiceError, EntityCollectionService, EntityServices, HttpUrlGenerator } from '@ngrx/data';
import { HttpClient } from '@angular/common/http';
import Catch from 'catch-finally-decorator';
import { skipUntil, skipWhile, take } from 'rxjs/operators';
import { Price } from 'src/app/shared/entities/price';
import { CouponsService } from './coupons.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  get price$(): Observable<Price> {
    return this.cartService.selectors$['price$'];
  }

  get weight$(): Observable<string> {
    return this.cartService.selectors$['weight$'];
  }

  get coupon() {
    return this.couponService.coupon;
  }

  private cartService: EntityCollectionService<CartItem>;

  get loading$(){
    return this.cartService.loading$;
  }

  get loaded$(){
    return this.cartService.loaded$;
  }

  constructor(
    entityServices: EntityServices, private couponService: CouponsService
  ) {
    this.cartService = entityServices.getEntityCollectionService("CartItem");
    setTimeout(() => this.init(),0);
  }

  async init(){
    await this.invalidateCart();
    this.cartService.entities$.subscribe(ents => this.upadeLocalStorage(ents.map(ent => ent.id)));
  }

  async invalidateCart() {
    let cartItemIds: string[] = [];
    this.cartService.setLoaded(false);
    if(typeof localStorage !== 'undefined' && localStorage){
      cartItemIds = JSON.parse(localStorage.getItem('cart-item-ids') ?? "[]");
    }
    const items = await this.getCartItemsByIds(cartItemIds);
    this.cartService.setLoaded(true);
  }

  upadeLocalStorage(cartItemIds: string[]){
    if(typeof localStorage !== 'undefined' && localStorage){
      localStorage.setItem('cart-item-ids', JSON.stringify(cartItemIds));
    }
  }

  getCartItemsByIds(cartItemIds: string[]){
    let query: any = this.getCartItemsQuery(cartItemIds);
    return this.cartService.getWithQuery({filter: JSON.stringify(query), coupon: this.coupon}).toPromise();

  }

  private getCartItemsQuery(cartItemIds: string[]) {
    return { _id: { $in: cartItemIds } };
  }

  getCartStream(){
    return this.cartService.entities$;
  }

  async getPendingCartItems(){
    await this.cartService.loaded$.pipe(skipWhile(val => val == false), take(1)).toPromise();
    let items =  await this.cartService.entities$.pipe(take(1)).toPromise();
    return this.filterPendingCartItems(items);
  }

  filterPendingCartItems(items: CartItem[]){
    return items.filter(item => item.status == CartItemStatus.PENDING);
  }


  async add(cartItem: CartItem) {
    let item = await this.cartService.add(cartItem).toPromise();
    await this.invalidateCart();
  }

  async update(cartItem: Partial<CartItem>){
    let item = await this.cartService.update(cartItem).toPromise();
    await this.invalidateCart();
  }

  async delete(cartItem: CartItem){
    await this.cartService.delete(cartItem, {isOptimistic: false}).toPromise();
    await this.invalidateCart();
  }

  clearCart(){
    this.cartService.clearCache();
    this.upadeLocalStorage([]);
  }

  getCartTotal(cartItems: CartItem[]){
    return -1;
  }

  public getCartItemCost(cartItem: CartItem, type: 'total' | 'subtotal' = 'total'){
    return cartItem.price[type];
  }

  getSaleTotal(cartItems: CartItem[]) {
    return this.cartService['total']

  }

}
