import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { DataServiceError, EntityCollectionService, EntityServices } from '@ngrx/data';
import { Observable, of } from 'rxjs';
import { CartItem } from 'src/app/shared/entities/cart-item';
import { MCAOrder } from 'src/app/shared/entities/order';
import Catch from 'catch-finally-decorator';
import { HttpErrorResponse } from '@angular/common/http';
import { CartService } from './cart.service';

@Injectable({
  providedIn: 'root'
})
export class OrderResolverService implements Resolve<{
  cartItems: Partial<CartItem>[],
  order:  MCAOrder;
}> {
  orderService: EntityCollectionService<MCAOrder>;

  constructor(private entityServices: EntityServices,
    private cartService: CartService,) {
      this.orderService = entityServices.getEntityCollectionService<MCAOrder>("MCAOrder");

    }

  @Catch(DataServiceError, (err: DataServiceError, ctx: OrderResolverService) => {
    let httpError: HttpErrorResponse = err.error;
    if(httpError.status == 404){
      return of(null);
    }
  })
  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<{ cartItems: Partial<CartItem>[]; order: MCAOrder; }> {
    let orderId = route.params['order-id'];
    let order = await this.orderService.getByKey(orderId).toPromise();
    let cartItems = order.cart;
    return {cartItems: cartItems, order: order};
  }
}
