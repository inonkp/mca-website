import { Inject, Injectable } from '@angular/core';
import { DataServiceError, EntityCollectionService, EntityServices } from '@ngrx/data';
import { CartItem } from 'src/app/shared/entities/cart-item';
import { Coupon } from 'src/app/shared/entities/copoun';
import { ShippingItem } from 'src/app/shared/entities/shipping-item';
import { CheckoutModule } from '../checkout.module';

@Injectable({
  providedIn: CheckoutModule
})
export class CouponsService {

  get coupon() {
    return this._coupon;
  }

  discountCodeService: EntityCollectionService<Coupon>;

  get loading$() {
    return this.discountCodeService.loading$;
  }

  constructor(entityServices: EntityServices, @Inject('COUPON') private _coupon: string) {
    this.discountCodeService = entityServices.getEntityCollectionService('DiscountCode');
   }

  getDiscountCode(code: string){
      return this.discountCodeService.getByKey(code).toPromise();
   }

   getInvalidCoupon(): Coupon {
    return {
      valid: false,
      code: ""
    }
   }

  hasCoupon() {
    return this.coupon != null;
  }
}
