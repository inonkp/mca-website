import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { EntityCollectionService, EntityServices } from '@ngrx/data';
import { Observable } from 'rxjs';
import { CartItem } from 'src/app/shared/entities/cart-item';
import { ShippingItem } from 'src/app/shared/entities/shipping-item';
import { ModelService } from 'src/app/shared/services/model.service';
import { CheckoutComponent } from '../checkout/checkout.component';
import { CartService } from './cart.service';

@Injectable({
  providedIn: 'root'
})
export class CheckoutResolverService implements Resolve<CartItem[]>{

  shippingService: EntityCollectionService<ShippingItem>;
  constructor(private cartService: CartService, private entityServices: EntityServices, private modelService: ModelService) {
    this.shippingService = entityServices.getEntityCollectionService<ShippingItem>('ShippingItem');
  }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<CartItem[]> {
    let cartItems = await this.cartService.getPendingCartItems();
    return cartItems;
  }

}
