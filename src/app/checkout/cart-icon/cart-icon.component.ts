import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CartItem } from 'src/app/shared/entities/cart-item';
import { CartService } from '../services/cart.service';
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons/faShoppingCart";
import { Router } from '@angular/router';

@Component({
  selector: 'mca-cart-icon',
  templateUrl: './cart-icon.component.html',
  styleUrls: ['./cart-icon.component.scss']
})
export class CartIconComponent implements OnInit, OnDestroy {

  get faShoppingCart(){
    return faShoppingCart;
  }

  @Input('color')
  color: string;

  items: CartItem[];
  subscription: Subscription;
  constructor(private router: Router, private cartService: CartService) {
    this.subscription = cartService.getCartStream().subscribe(items => this.items = cartService.filterPendingCartItems(items));
  }

  ngOnInit(): void {
  }

  routeToShoppingCart(){
    this.router.navigate(['/cart']);
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
