import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { faPlus } from "@fortawesome/free-solid-svg-icons/faPlus";
import { faMinus  } from "@fortawesome/free-solid-svg-icons/faMinus";

@Component({
  selector: 'mca-cart-quantity',
  templateUrl: './cart-quantity.component.html',
  styleUrls: ['./cart-quantity.component.scss']
})
export class CartQuantityComponent implements OnInit {

  @Output('add-to-cart')
  addToCart: EventEmitter<void>;

  @Output('subtract-from-cart')
  subtractFromCart: EventEmitter<void>;

  @Input('quantity')
  quantity: number;

  @Input('disabled')
  disabled: boolean;

  get faMinus(){
    return faMinus;
  }

  get faPlus(){
    return faPlus;
  }

  constructor() {
    this.addToCart = new EventEmitter();
    this.subtractFromCart = new EventEmitter();
  }

  ngOnInit(): void {
  }

}
