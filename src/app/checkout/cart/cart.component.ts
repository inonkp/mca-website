import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import {
  EntityCollectionService,
  EntityCollectionServiceBase,
  EntityServices
} from "@ngrx/data";
import {CartItem} from 'src/app/shared/entities/cart-item';
import {ActivatedRoute, Router} from '@angular/router';
import { CartService } from '../services/cart.service';
import { Observable } from 'rxjs';
import { CartItemRowComponent } from './cart-item-row/cart-item-row.component';
import { filter, take } from 'rxjs/operators';
import { ProductsService } from 'src/app/catalog/services/products.service';
import { Product } from 'src/app/shared/entities/product';
import { McaSeoService } from 'src/app/shared/services/mca-seo.service';

@Component({
  selector: 'mca-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})

export class CartComponent implements OnInit {

  cartItems: CartItem[];

  readonly headerWidth = {
    product: "40%",
    price: "15%",
    quantity: "25%",
    total: "15%",
    close: "5%"
  }

  loaded: boolean;

  constructor(activatedRoute: ActivatedRoute, public cartService: CartService, private seo: McaSeoService) {
    seo.noindex();
    seo.setTitle('Cart');
    this.loaded = false;
    this.cartItems = activatedRoute.snapshot.data.cartItems;
   }

  ngOnInit(): void {

  }

  async reloadCart(){
    this.cartItems = await this.cartService.getPendingCartItems();
  }

}
