import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CartItemTitleComponent } from './cart-item-title.component';

describe('CartItemTitleComponent', () => {
  let component: CartItemTitleComponent;
  let fixture: ComponentFixture<CartItemTitleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CartItemTitleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CartItemTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
