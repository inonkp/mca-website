import { Component, Input, OnInit } from '@angular/core';
import { CartItem } from 'src/app/shared/entities/cart-item';
import { ModelItem } from 'src/app/shared/entities/model-item';
import { Product } from 'src/app/shared/entities/product';
import { ImageResizerService } from 'src/app/shared/services/image-resizer.service';
import { ModelService } from 'src/app/shared/services/model.service';

@Component({
  selector: 'mca-cart-item-title',
  templateUrl: './cart-item-title.component.html',
  styleUrls: ['./cart-item-title.component.scss']
})
export class CartItemTitleComponent implements OnInit {
  productsModel: ModelItem[];
  @Input('cart-item')
  cartItem: CartItem;

  @Input('product')
  product: Partial<Product>;

  thumbnail: string;
  options: string;
  title: string;
  fullImage: string;

  constructor(private resizerService: ImageResizerService, private modelService: ModelService) {
    this.productsModel = [];
   }

  ngOnInit(): void {
    this.productsModel = this.modelService.model;
    this.initThumbnail();
    this.initTitle();
    this.initOptions();

  }

  initThumbnail(){
    this.thumbnail = this.product.catalog_img;
    this.fullImage = this.product.image;
  }

  initTitle(){
    this.title = this.product.title;
  }

  initOptions(){
    let model = this.modelService.getModelItemById(this.cartItem.modelId);

    this.options = model.summary;
  }

  openImageModal(){
    this.resizerService.openLargeImgModal(this.fullImage);
  }

}
