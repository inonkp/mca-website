import { Component, EventEmitter, Input, OnInit, Output, ViewChildren } from '@angular/core';
import { ProductsService } from 'src/app/catalog/services/products.service';
import { CartItem } from 'src/app/shared/entities/cart-item';
import { Product } from 'src/app/shared/entities/product';
import { CartService } from '../../services/cart.service';
import { faTimes } from "@fortawesome/free-solid-svg-icons/faTimes";
import { ModelService } from 'src/app/shared/services/model.service';
import { ModelItem } from 'src/app/shared/entities/model-item';

@Component({
  selector: 'mca-cart-item-row',
  templateUrl: './cart-item-row.component.html',
  styleUrls: ['./cart-item-row.component.scss']
})
export class CartItemRowComponent implements OnInit {

  modelItem: ModelItem | null;

  @Input('cart-item')
  set cartItem(item: CartItem){
    this._cartItem = item;
    this.modelItem = this.modelService.getModelItemById(item.modelId);
  }

  get cartItem() {
    return this._cartItem;
  }

  @Output('update')
  updade: EventEmitter<void>;

  @Input('header-width')
  readonly headerWidth;

  loaded: Promise<Product>;

  _cartItem: CartItem;

  @Input('product')
  _product: Partial<Product>;

  get faTimes(){
    return faTimes;
  }

  constructor(private productService: ProductsService, public cartService: CartService,
    private modelService: ModelService) {
    this.updade = new EventEmitter(true);
   }

  ngOnInit(): void {
  }

  getTotal(){
    return this.cartService.getCartItemCost(this.cartItem)
  }

  async addQuantity(){
    let newQuantity = this._cartItem.quantity + 1;
    await this.cartService.update({
      id: this._cartItem.id,
      quantity: newQuantity
    });
    this.updade.emit();
  }

  async subtractQuantity(){
    if(this._cartItem.quantity <= 1){
      return;
    }
    let newQuantity = this._cartItem.quantity - 1;
    await this.cartService.update({
      id: this._cartItem.id,
      quantity: newQuantity
    });
    this.updade.emit();
  }

  async deleteItem(){
    await this.cartService.delete(this._cartItem);
    this.updade.emit();
  }

}
