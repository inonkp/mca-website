import { AfterViewInit, Component, OnInit, Inject, ElementRef, Input, Output, EventEmitter, NgZone } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { CartItem } from 'src/app/shared/entities/cart-item';
import { Product } from 'src/app/shared/entities/product';
import { loadScript } from "@paypal/paypal-js";
import Catch from 'catch-finally-decorator';
import { ShippingItem } from 'src/app/shared/entities/shipping-item';
import { EntityCollectionDataService, EntityDataService } from '@ngrx/data';
import { ModelService } from 'src/app/shared/services/model.service';
import { Address } from 'src/app/shared/entities/address';
import { faSpinner } from '@fortawesome/free-solid-svg-icons/faSpinner';
import { environment } from 'src/environments/environment';


type PayPalShippingOption = {
  id: string;
  label: string;
  type: "SHIPPING";
  selected: boolean;
  amount: {
      value: string;
      currency_code: string;
  }
}



@Component({
  selector: 'mca-paypal-button',
  templateUrl: './paypal-button.component.html',
  styleUrls: ['./paypal-button.component.scss']
})
export class PaypalButtonComponent implements OnInit, AfterViewInit {

  loading: boolean;

  @Input('total')
  total: string;

  @Input('item-total')
  itemTotal: string;

  @Input('shipping-total')
  shippingTotal: string;

  @Output('payment-approved')
  onPaymentEmitter: EventEmitter<any>;

  @Input('disable')
  set disabled(val: boolean){
    if(val){
      this._disabled = true;
      this.disable();
    }else{
      this._disabled = false;
      this.enable();
    }
  }

  get disabled(){
    return this._disabled;
  }

  @Input('address')
  set address(val: Address){
    this._address = val;
  }

  get address(){
    return this._address;
  }

  _address: Address;
  get faSpinner(){
    return faSpinner;
  }

  @Input("valid")
  set valid(val: boolean){
    this._valid = val;
    if(val){
      this.enable();
    }else{
      this.disable();
    }
  }
  _valid: boolean;
  _disabled: boolean;

  enable: () => Promise<void>;
  disable: () => Promise<void>;

  @Output('click')
  clickEmitter: EventEmitter<void>;

  constructor(private zone: NgZone) {
    this._valid = false;
    this.clickEmitter = new EventEmitter();
    this.enable = () => Promise.resolve();
    this.disable = () => Promise.resolve();
    this.loading = true;
    this.onPaymentEmitter = new EventEmitter();
  }

  ngOnInit(): void {
  }

  async loadScript(){
    this.loading = true

    const _this: PaypalButtonComponent = this;
    let paypal = await loadScript({
      "client-id": environment.paypal_client_id,
      currency: "USD"
    });
    this.loading = false;
    paypal.Buttons({
      onInit: function(data, actions) {

        // Disable the buttons
        actions.disable();
        _this.enable = actions.enable;
        _this.disable = actions.disable;
      },

      createOrder: function(data, actions) {
        return actions.order.create({
          application_context: {
            brand_name: "Multi Canvas Art",
            shipping_preference: 'SET_PROVIDED_ADDRESS',

          },
          purchase_units: [{
              amount: {
                value: _this.total
              },
              shipping: {
                name: {
                  full_name: _this._address['first-name'] + " " + _this._address['last-name']
                },
                address: {
                  address_line_1: _this._address.address1,
                  address_line_2: _this._address.address2,
                  admin_area_1: _this._address.city,
                  admin_area_2: _this._address.state,
                  postal_code: _this._address.zipcode,
                  country_code: _this.address.country_code
                }
              }
          }]
        });
      },
      async onApprove(data, actions){
        await actions.order.capture();
        _this.zone.run(() => _this.onPaymentEmitter.emit(data));
      }
    }).render("#paypal-button");
  }

  ngAfterViewInit(){
    this.loadScript();

  }


  mapShippingOptions(shippingItems: ShippingItem[]): PayPalShippingOption[]{
    return shippingItems.map((item,index) => {
      return {
        id: item.id,
        label: item.shipping_method + " - " + item.shipping_method_description,
        type: "SHIPPING",
        selected: index==0,
        amount: {
          currency_code: "USD",
          value: item.price.total
        }
      }
    })
  }
}
