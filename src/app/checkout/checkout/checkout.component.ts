import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormBuilder, FormGroup, Validators } from '@angular/forms';
import { EntityCollectionDataService, EntityCollectionService, EntityDataService, EntityServices } from '@ngrx/data';
import { ProductsService } from 'src/app/catalog/services/products.service';
import { CartItem } from 'src/app/shared/entities/cart-item';
import { MCAOrder } from 'src/app/shared/entities/order';
import { Product } from 'src/app/shared/entities/product';
import { ShippingItem } from 'src/app/shared/entities/shipping-item';
import { ModelService } from 'src/app/shared/services/model.service';
import { CartService } from '../services/cart.service';
import { faSpinner } from '@fortawesome/free-solid-svg-icons/faSpinner';
import { ActivatedRoute, Router } from '@angular/router';
import { McaSeoService } from 'src/app/shared/services/mca-seo.service';
import { Coupon } from 'src/app/shared/entities/copoun';
import { PricingService } from 'src/app/catalog/services/pricing.service';
import { merge, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PaymentTotalComponent } from './payment-total/payment-total.component';
var checkout = {
  loading$: null
}
@Component({
  selector: 'mca-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
  providers:[
    {
      provide: "CHECKOUT",
      useValue: {
        registerLoader: (loader$: Observable<boolean>) => checkout.loading$ = 
          checkout.loading$ ? merge(checkout.loading$, loader$) : loader$,
        get loading$() {
          return checkout.loading$
        }
      }
    }
  ]
})
export class CheckoutComponent implements OnInit{
  static readonly DEFAULT_COUNTRY = "US";
  readonly checkout = checkout;

  @ViewChild('mcaTotal')
  mcaTotal: PaymentTotalComponent;

  get development() {
    return !environment.production;
  }
  get faSpinner(){
    return faSpinner;
  }

  cartItems: CartItem[];
  weight: number;

  orderService: EntityCollectionService<MCAOrder>;

  constructor(public cartService: CartService, entityServices: EntityServices,
    private router: Router, activatedRoute: ActivatedRoute, 
    seo: McaSeoService) {
      seo.noindex();
      seo.setTitle('Checkout');
      this.orderService = entityServices.getEntityCollectionService<MCAOrder>("MCAOrder");
      this.cartItems = activatedRoute.snapshot.data.data;

    }

  getTotalPriceLabel(itemCost: number, shippingCost: number) {
    return (itemCost + shippingCost).toFixed(2);
  }

  ngOnInit(): void {
  }

  async postOrder(paymentData: any, address: any, coupon: string,
    shippingItem: ShippingItem){
    
    let order = await this.orderService.add({
      address: address,
      shipping: shippingItem,
      coupon: coupon,
      cart: this.cartItems,
      cart_cost: this.mcaTotal.itemCost,
      payment_method: "paypal",
      payment_details: paymentData,
      status: "processing",
      price: this.mcaTotal.total
    }).toPromise();
    this.cartService.clearCart();
    this.router.navigate(["order/" + order.id])
  }

}