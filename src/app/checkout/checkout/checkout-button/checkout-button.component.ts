import { Component, OnInit } from '@angular/core';
import { faArrowRight } from "@fortawesome/free-solid-svg-icons/faArrowRight";

@Component({
  selector: 'mca-checkout-button',
  templateUrl: './checkout-button.component.html',
  styleUrls: ['./checkout-button.component.scss']
})
export class CheckoutButtonComponent implements OnInit {

  get faArrowRight(){
    return faArrowRight;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
