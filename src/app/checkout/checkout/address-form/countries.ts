export class Countries{
  /**
   * based on the continents Asia,
 North America,
 Europe,
 South America,
 Australia
   */

  static readonly MAJOR_COUNTRIES = [
    {
      "country_code":"US",
      "country":"United States",
      "continent":"North America"
    },
    {
      "country_code":"GB",
      "country":"United Kingdom",
      "continent":"Europe"
   },
    {
      "country_code":"AU",
      "country":"Australia",
      "continent":"Oceania"
   },
  ];

  static readonly COUNTRIES = [
    {
       "country_code":"AF",
       "country":"Afghanistan, Islamic Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"AL",
       "country":"Albania, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"AS",
       "country":"American Samoa",
       "continent":"Oceania"
    },
    {
       "country_code":"AD",
       "country":"Andorra, Principality of",
       "continent":"Europe"
    },
    {
       "country_code":"AG",
       "country":"Antigua and Barbuda",
       "continent":"North America"
    },
    {
       "country_code":"AZ",
       "country":"Azerbaijan, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"AZ",
       "country":"Azerbaijan, Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"AR",
       "country":"Argentina, Argentine Republic",
       "continent":"South America"
    },
    {
       "country_code":"AT",
       "country":"Austria, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"BS",
       "country":"Bahamas, Commonwealth of the",
       "continent":"North America"
    },
    {
       "country_code":"BH",
       "country":"Bahrain, Kingdom of",
       "continent":"Asia"
    },
    {
       "country_code":"BD",
       "country":"Bangladesh, People's Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"AM",
       "country":"Armenia, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"AM",
       "country":"Armenia, Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"BB",
       "country":"Barbados",
       "continent":"North America"
    },
    {
       "country_code":"BE",
       "country":"Belgium, Kingdom of",
       "continent":"Europe"
    },
    {
       "country_code":"BM",
       "country":"Bermuda",
       "continent":"North America"
    },
    {
       "country_code":"BT",
       "country":"Bhutan, Kingdom of",
       "continent":"Asia"
    },
    {
       "country_code":"BO",
       "country":"Bolivia, Republic of",
       "continent":"South America"
    },
    {
       "country_code":"BA",
       "country":"Bosnia and Herzegovina",
       "continent":"Europe"
    },
    {
       "country_code":"BR",
       "country":"Brazil, Federative Republic of",
       "continent":"South America"
    },
    {
       "country_code":"BZ",
       "country":"Belize",
       "continent":"North America"
    },
    {
       "country_code":"IO",
       "country":"British Indian Ocean Territory (Chagos Archipelago)",
       "continent":"Asia"
    },
    {
       "country_code":"SB",
       "country":"Solomon Islands",
       "continent":"Oceania"
    },
    {
       "country_code":"VG",
       "country":"British Virgin Islands",
       "continent":"North America"
    },
    {
       "country_code":"BN",
       "country":"Brunei Darussalam",
       "continent":"Asia"
    },
    {
       "country_code":"BG",
       "country":"Bulgaria, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"MM",
       "country":"Myanmar, Union of",
       "continent":"Asia"
    },
    {
       "country_code":"BY",
       "country":"Belarus, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"KH",
       "country":"Cambodia, Kingdom of",
       "continent":"Asia"
    },
    {
       "country_code":"CA",
       "country":"Canada",
       "continent":"North America"
    },
    {
       "country_code":"KY",
       "country":"Cayman Islands",
       "continent":"North America"
    },
    {
       "country_code":"LK",
       "country":"Sri Lanka, Democratic Socialist Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"CL",
       "country":"Chile, Republic of",
       "continent":"South America"
    },
    {
       "country_code":"CN",
       "country":"China, People's Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"TW",
       "country":"Taiwan",
       "continent":"Asia"
    },
    {
       "country_code":"CX",
       "country":"Christmas Island",
       "continent":"Asia"
    },
    {
       "country_code":"CC",
       "country":"Cocos (Keeling) Islands",
       "continent":"Asia"
    },
    {
       "country_code":"CO",
       "country":"Colombia, Republic of",
       "continent":"South America"
    },
    {
       "country_code":"CK",
       "country":"Cook Islands",
       "continent":"Oceania"
    },
    {
       "country_code":"CR",
       "country":"Costa Rica, Republic of",
       "continent":"North America"
    },
    {
       "country_code":"HR",
       "country":"Croatia, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"CU",
       "country":"Cuba, Republic of",
       "continent":"North America"
    },
    {
       "country_code":"CY",
       "country":"Cyprus, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"CY",
       "country":"Cyprus, Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"CZ",
       "country":"Czech Republic",
       "continent":"Europe"
    },
    {
       "country_code":"DK",
       "country":"Denmark, Kingdom of",
       "continent":"Europe"
    },
    {
       "country_code":"DM",
       "country":"Dominica, Commonwealth of",
       "continent":"North America"
    },
    {
       "country_code":"DO",
       "country":"Dominican Republic",
       "continent":"North America"
    },
    {
       "country_code":"EC",
       "country":"Ecuador, Republic of",
       "continent":"South America"
    },
    {
       "country_code":"SV",
       "country":"El Salvador, Republic of",
       "continent":"North America"
    },
    {
       "country_code":"EE",
       "country":"Estonia, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"FO",
       "country":"Faroe Islands",
       "continent":"Europe"
    },
    {
       "country_code":"FK",
       "country":"Falkland Islands (Malvinas)",
       "continent":"South America"
    },
    {
       "country_code":"FJ",
       "country":"Fiji, Republic of the Fiji Islands",
       "continent":"Oceania"
    },
    {
       "country_code":"FI",
       "country":"Finland, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"AX",
       "country":"Åland Islands",
       "continent":"Europe"
    },
    {
       "country_code":"FR",
       "country":"France, French Republic",
       "continent":"Europe"
    },
    {
       "country_code":"GF",
       "country":"French Guiana",
       "continent":"South America"
    },
    {
       "country_code":"PF",
       "country":"French Polynesia",
       "continent":"Oceania"
    },
    {
       "country_code":"GE",
       "country":"Georgia",
       "continent":"Europe"
    },
    {
       "country_code":"GE",
       "country":"Georgia",
       "continent":"Asia"
    },
    {
       "country_code":"PS",
       "country":"Palestinian Territory, Occupied",
       "continent":"Asia"
    },
    {
       "country_code":"DE",
       "country":"Germany, Federal Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"GI",
       "country":"Gibraltar",
       "continent":"Europe"
    },
    {
       "country_code":"KI",
       "country":"Kiribati, Republic of",
       "continent":"Oceania"
    },
    {
       "country_code":"GR",
       "country":"Greece, Hellenic Republic",
       "continent":"Europe"
    },
    {
       "country_code":"GL",
       "country":"Greenland",
       "continent":"North America"
    },
    {
       "country_code":"GD",
       "country":"Grenada",
       "continent":"North America"
    },
    {
       "country_code":"GP",
       "country":"Guadeloupe",
       "continent":"North America"
    },
    {
       "country_code":"GU",
       "country":"Guam",
       "continent":"Oceania"
    },
    {
       "country_code":"GT",
       "country":"Guatemala, Republic of",
       "continent":"North America"
    },
    {
       "country_code":"GY",
       "country":"Guyana, Co-operative Republic of",
       "continent":"South America"
    },
    {
       "country_code":"HT",
       "country":"Haiti, Republic of",
       "continent":"North America"
    },
    {
       "country_code":"VA",
       "country":"Holy See (Vatican City State)",
       "continent":"Europe"
    },
    {
       "country_code":"HN",
       "country":"Honduras, Republic of",
       "continent":"North America"
    },
    {
       "country_code":"HK",
       "country":"Hong Kong, Special Administrative Region of China",
       "continent":"Asia"
    },
    {
       "country_code":"HU",
       "country":"Hungary, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"IS",
       "country":"Iceland, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"IN",
       "country":"India, Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"ID",
       "country":"Indonesia, Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"IR",
       "country":"Iran, Islamic Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"IQ",
       "country":"Iraq, Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"IE",
       "country":"Ireland",
       "continent":"Europe"
    },
    {
       "country_code":"IL",
       "country":"Israel, State of",
       "continent":"Asia"
    },
    {
       "country_code":"IT",
       "country":"Italy, Italian Republic",
       "continent":"Europe"
    },
    {
       "country_code":"JM",
       "country":"Jamaica",
       "continent":"North America"
    },
    {
       "country_code":"JP",
       "country":"Japan",
       "continent":"Asia"
    },
    {
       "country_code":"KZ",
       "country":"Kazakhstan, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"KZ",
       "country":"Kazakhstan, Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"JO",
       "country":"Jordan, Hashemite Kingdom of",
       "continent":"Asia"
    },
    {
       "country_code":"KP",
       "country":"Korea, Democratic People's Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"KR",
       "country":"Korea, Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"KW",
       "country":"Kuwait, State of",
       "continent":"Asia"
    },
    {
       "country_code":"KG",
       "country":"Kyrgyz Republic",
       "continent":"Asia"
    },
    {
       "country_code":"LA",
       "country":"Lao People's Democratic Republic",
       "continent":"Asia"
    },
    {
       "country_code":"LB",
       "country":"Lebanon, Lebanese Republic",
       "continent":"Asia"
    },
    {
       "country_code":"LV",
       "country":"Latvia, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"LI",
       "country":"Liechtenstein, Principality of",
       "continent":"Europe"
    },
    {
       "country_code":"LT",
       "country":"Lithuania, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"LU",
       "country":"Luxembourg, Grand Duchy of",
       "continent":"Europe"
    },
    {
       "country_code":"MO",
       "country":"Macao, Special Administrative Region of China",
       "continent":"Asia"
    },
    {
       "country_code":"MY",
       "country":"Malaysia",
       "continent":"Asia"
    },
    {
       "country_code":"MV",
       "country":"Maldives, Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"MT",
       "country":"Malta, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"MQ",
       "country":"Martinique",
       "continent":"North America"
    },
    {
       "country_code":"MX",
       "country":"Mexico, United Mexican States",
       "continent":"North America"
    },
    {
       "country_code":"MC",
       "country":"Monaco, Principality of",
       "continent":"Europe"
    },
    {
       "country_code":"MN",
       "country":"Mongolia",
       "continent":"Asia"
    },
    {
       "country_code":"MD",
       "country":"Moldova, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"ME",
       "country":"Montenegro, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"MS",
       "country":"Montserrat",
       "continent":"North America"
    },
    {
       "country_code":"OM",
       "country":"Oman, Sultanate of",
       "continent":"Asia"
    },
    {
       "country_code":"NR",
       "country":"Nauru, Republic of",
       "continent":"Oceania"
    },
    {
       "country_code":"NP",
       "country":"Nepal, State of",
       "continent":"Asia"
    },
    {
       "country_code":"NL",
       "country":"Netherlands, Kingdom of the",
       "continent":"Europe"
    },
    {
       "country_code":"AN",
       "country":"Netherlands Antilles",
       "continent":"North America"
    },
    {
       "country_code":"CW",
       "country":"Curaçao",
       "continent":"North America"
    },
    {
       "country_code":"AW",
       "country":"Aruba",
       "continent":"North America"
    },
    {
       "country_code":"SX",
       "country":"Sint Maarten (Netherlands)",
       "continent":"North America"
    },
    {
       "country_code":"BQ",
       "country":"Bonaire, Sint Eustatius and Saba",
       "continent":"North America"
    },
    {
       "country_code":"NC",
       "country":"New Caledonia",
       "continent":"Oceania"
    },
    {
       "country_code":"VU",
       "country":"Vanuatu, Republic of",
       "continent":"Oceania"
    },
    {
       "country_code":"NZ",
       "country":"New Zealand",
       "continent":"Oceania"
    },
    {
       "country_code":"NI",
       "country":"Nicaragua, Republic of",
       "continent":"North America"
    },
    {
       "country_code":"NU",
       "country":"Niue",
       "continent":"Oceania"
    },
    {
       "country_code":"NF",
       "country":"Norfolk Island",
       "continent":"Oceania"
    },
    {
       "country_code":"NO",
       "country":"Norway, Kingdom of",
       "continent":"Europe"
    },
    {
       "country_code":"MP",
       "country":"Northern Mariana Islands, Commonwealth of the",
       "continent":"Oceania"
    },
    {
       "country_code":"UM",
       "country":"United States Minor Outlying Islands",
       "continent":"Oceania"
    },
    {
       "country_code":"UM",
       "country":"United States Minor Outlying Islands",
       "continent":"North America"
    },
    {
       "country_code":"FM",
       "country":"Micronesia, Federated States of",
       "continent":"Oceania"
    },
    {
       "country_code":"MH",
       "country":"Marshall Islands, Republic of the",
       "continent":"Oceania"
    },
    {
       "country_code":"PW",
       "country":"Palau, Republic of",
       "continent":"Oceania"
    },
    {
       "country_code":"PK",
       "country":"Pakistan, Islamic Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"PA",
       "country":"Panama, Republic of",
       "continent":"North America"
    },
    {
       "country_code":"PG",
       "country":"Papua New Guinea, Independent State of",
       "continent":"Oceania"
    },
    {
       "country_code":"PY",
       "country":"Paraguay, Republic of",
       "continent":"South America"
    },
    {
       "country_code":"PE",
       "country":"Peru, Republic of",
       "continent":"South America"
    },
    {
       "country_code":"PH",
       "country":"Philippines, Republic of the",
       "continent":"Asia"
    },
    {
       "country_code":"PN",
       "country":"Pitcairn Islands",
       "continent":"Oceania"
    },
    {
       "country_code":"PL",
       "country":"Poland, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"PT",
       "country":"Portugal, Portuguese Republic",
       "continent":"Europe"
    },
    {
       "country_code":"TL",
       "country":"Timor-Leste, Democratic Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"PR",
       "country":"Puerto Rico, Commonwealth of",
       "continent":"North America"
    },
    {
       "country_code":"QA",
       "country":"Qatar, State of",
       "continent":"Asia"
    },
    {
       "country_code":"RO",
       "country":"Romania",
       "continent":"Europe"
    },
    {
       "country_code":"RU",
       "country":"Russian Federation",
       "continent":"Europe"
    },
    {
       "country_code":"RU",
       "country":"Russian Federation",
       "continent":"Asia"
    },
    {
       "country_code":"BL",
       "country":"Saint Barthelemy",
       "continent":"North America"
    },
    {
       "country_code":"KN",
       "country":"Saint Kitts and Nevis, Federation of",
       "continent":"North America"
    },
    {
       "country_code":"AI",
       "country":"Anguilla",
       "continent":"North America"
    },
    {
       "country_code":"LC",
       "country":"Saint Lucia",
       "continent":"North America"
    },
    {
       "country_code":"MF",
       "country":"Saint Martin",
       "continent":"North America"
    },
    {
       "country_code":"PM",
       "country":"Saint Pierre and Miquelon",
       "continent":"North America"
    },
    {
       "country_code":"VC",
       "country":"Saint Vincent and the Grenadines",
       "continent":"North America"
    },
    {
       "country_code":"SM",
       "country":"San Marino, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"SA",
       "country":"Saudi Arabia, Kingdom of",
       "continent":"Asia"
    },
    {
       "country_code":"RS",
       "country":"Serbia, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"SG",
       "country":"Singapore, Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"SK",
       "country":"Slovakia (Slovak Republic)",
       "continent":"Europe"
    },
    {
       "country_code":"VN",
       "country":"Vietnam, Socialist Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"SI",
       "country":"Slovenia, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"ES",
       "country":"Spain, Kingdom of",
       "continent":"Europe"
    },
    {
       "country_code":"SR",
       "country":"Suriname, Republic of",
       "continent":"South America"
    },
    {
       "country_code":"SJ",
       "country":"Svalbard & Jan Mayen Islands",
       "continent":"Europe"
    },
    {
       "country_code":"SE",
       "country":"Sweden, Kingdom of",
       "continent":"Europe"
    },
    {
       "country_code":"CH",
       "country":"Switzerland, Swiss Confederation",
       "continent":"Europe"
    },
    {
       "country_code":"SY",
       "country":"Syrian Arab Republic",
       "continent":"Asia"
    },
    {
       "country_code":"TJ",
       "country":"Tajikistan, Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"TH",
       "country":"Thailand, Kingdom of",
       "continent":"Asia"
    },
    {
       "country_code":"TK",
       "country":"Tokelau",
       "continent":"Oceania"
    },
    {
       "country_code":"TO",
       "country":"Tonga, Kingdom of",
       "continent":"Oceania"
    },
    {
       "country_code":"TT",
       "country":"Trinidad and Tobago, Republic of",
       "continent":"North America"
    },
    {
       "country_code":"AE",
       "country":"United Arab Emirates",
       "continent":"Asia"
    },
    {
       "country_code":"TR",
       "country":"Turkey, Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"TR",
       "country":"Turkey, Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"TM",
       "country":"Turkmenistan",
       "continent":"Asia"
    },
    {
       "country_code":"TC",
       "country":"Turks and Caicos Islands",
       "continent":"North America"
    },
    {
       "country_code":"TV",
       "country":"Tuvalu",
       "continent":"Oceania"
    },
    {
       "country_code":"UA",
       "country":"Ukraine",
       "continent":"Europe"
    },
    {
       "country_code":"MK",
       "country":"Macedonia, The Former Yugoslav Republic of",
       "continent":"Europe"
    },
    {
       "country_code":"GG",
       "country":"Guernsey, Bailiwick of",
       "continent":"Europe"
    },
    {
       "country_code":"JE",
       "country":"Jersey, Bailiwick of",
       "continent":"Europe"
    },
    {
       "country_code":"IM",
       "country":"Isle of Man",
       "continent":"Europe"
    },
    {
       "country_code":"VI",
       "country":"United States Virgin Islands",
       "continent":"North America"
    },
    {
       "country_code":"UY",
       "country":"Uruguay, Eastern Republic of",
       "continent":"South America"
    },
    {
       "country_code":"UZ",
       "country":"Uzbekistan, Republic of",
       "continent":"Asia"
    },
    {
       "country_code":"VE",
       "country":"Venezuela, Bolivarian Republic of",
       "continent":"South America"
    },
    {
       "country_code":"WF",
       "country":"Wallis and Futuna",
       "continent":"Oceania"
    },
    {
       "country_code":"WS",
       "country":"Samoa, Independent State of",
       "continent":"Oceania"
    },
    {
       "country_code":"YE",
       "country":"Yemen",
       "continent":"Asia"
    },
    {
       "country_code":"XX",
       "country":"Disputed Territory",
       "continent":"Oceania"
    },
    {
       "country_code":"XE",
       "country":"Iraq-Saudi Arabia Neutral Zone",
       "continent":"Asia"
    },
    {
       "country_code":"XD",
       "country":"United Nations Neutral Zone",
       "continent":"Asia"
    },
    {
       "country_code":"XS",
       "country":"Spratly Islands",
       "continent":"Asia"
    }
 ];
}
