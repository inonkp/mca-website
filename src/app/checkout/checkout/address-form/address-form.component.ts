import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Countries } from './countries';

@Component({
  selector: 'mca-address-form',
  templateUrl: './address-form.component.html',
  styleUrls: ['./address-form.component.scss']
})
export class AddressFormComponent implements OnInit, OnDestroy{

  readonly MAJOR_COUNTRIES = Countries.MAJOR_COUNTRIES;
  readonly COUNTRIES = Countries.COUNTRIES;

  @Output('country-change')
  countryChangeEmitter: EventEmitter<string>;

  submitted: boolean;

  get valid() {
    return this.addressFormGroup.valid;
  }

  get value() {
    return this.addressFormGroup.value;
  }

  @Input('disable')
  set disabled(val: boolean){
    if(val){
      this.addressFormGroup.disable({emitEvent: false});
    }else{
      this.addressFormGroup.enable({emitEvent: false});
    }
  }

  countryCodeSubscription: Subscription;

  addressFormGroup: UntypedFormGroup;
  constructor(private fb: UntypedFormBuilder) {
    this.countryChangeEmitter = new EventEmitter();
    this.submitted = false;
    this.addressFormGroup = this.fb.group({
      "first-name": ["", Validators.required],
      "last-name": ["", Validators.required],
      phone: ["", Validators.required],
      email: ["", Validators.required],
      address1: ["", Validators.required],
      address2: [""],
      city: ["", Validators.required],
      state: ["", Validators.required],
      zipcode: ["", Validators.required],
      country_code: ["", Validators.required],
      country: [""]
    });
  }


  ngOnInit(): void {

    this.countryCodeSubscription = this.addressFormGroup.get('country_code').valueChanges.subscribe(
      value => {
        this.addressFormGroup.get('country').setValue(this.MAJOR_COUNTRIES.concat(this.COUNTRIES).find(val => val.country_code==value).country);
        this.countryChangeEmitter.emit(value)
      });
  }

  ngOnDestroy(){
    this.countryCodeSubscription.unsubscribe();
  }


  onSubmit(){
    this.addressFormGroup.markAllAsTouched();
    this.submitted = true;
  }

}
