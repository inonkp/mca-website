import { HttpClient } from '@angular/common/http';
import { Component, Inject, Input, OnDestroy, OnInit } from '@angular/core';
import { DataServiceError, EntityCollectionService, EntityServices } from '@ngrx/data';
import Catch from 'catch-finally-decorator';
import { FormControlStatus, UntypedFormControl } from '@angular/forms';
import { Coupon } from 'src/app/shared/entities/copoun';
import { Subscription } from 'rxjs';
import { CouponsService } from '../../services/coupons.service';
import { ShippingItem } from 'src/app/shared/entities/shipping-item';
import { CartItem } from 'src/app/shared/entities/cart-item';
import { skipUntil, skipWhile, take, tap } from 'rxjs/operators';
import { query } from 'express';

@Component({
  selector: 'mca-discount-code',
  templateUrl: './discount-code.component.html',
  styleUrls: ['./discount-code.component.scss']
})
export class DiscountCodeComponent implements OnInit, OnDestroy {

  @Input('disable')
  set disabled(val: boolean) {
    if(val){
      this.formControl.disable();
    }else {
      this.formControl.enable();
    }
  }

  get coupon() {
    return this.discountCode;
  }

  formControl: UntypedFormControl;
  discountCode: Coupon;
  error: boolean;
  constructor(@Inject('CHECKOUT') public checkout, private couponsService: CouponsService) {
    checkout.registerLoader(couponsService.loading$)
    this.formControl = new UntypedFormControl(couponsService.coupon);
    this.invalidateCode();
    this.error = false;
  }

  ngOnInit(): void {
  }

  async onError() {
    await this.formControl.statusChanges.pipe(
      skipWhile((value: FormControlStatus) => value == "DISABLED"), take(1)).toPromise();
    this.formControl.setErrors({'bad-code': "Bad Code"});
  }

  invalidateCode(){
    this.discountCode = this.couponsService.getInvalidCoupon()
  }

  @Catch(DataServiceError, (err: DataServiceError, ctx: DiscountCodeComponent) => {
    ctx.onError()
  })
  async submitCode(val: string){
    await this.couponsService.getDiscountCode(val);
    var url = new URL(window.location.href);
    if(url.searchParams.get('coupon-code') != val) {
      url.searchParams.append("coupon-code", val);
    }
    window.location.href = url.href;
  }

  ngOnDestroy(): void {
  }
}
