import { Component, Input, OnInit } from '@angular/core';
import { CartItem } from 'src/app/shared/entities/cart-item';
import { Product } from 'src/app/shared/entities/product';

@Component({
  selector: 'mca-mini-cart-item',
  templateUrl: './mini-cart-item.component.html',
  styleUrls: ['./mini-cart-item.component.scss']
})
export class MiniCartItemComponent implements OnInit {

  @Input('cart-item')
  cartItem: CartItem;


  constructor() { }

  ngOnInit(): void {
  }

  getTotal(){
    // return (this.cartItem.options.price * this.cartItem.quantity).toFixed(2)
  }

}
