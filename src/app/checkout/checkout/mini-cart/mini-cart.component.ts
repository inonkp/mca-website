import { Component, Input, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { CartItem } from 'src/app/shared/entities/cart-item';
import { Coupon } from 'src/app/shared/entities/copoun';
import { Product } from 'src/app/shared/entities/product';
import { CartService } from '../../services/cart.service';

@Component({
  selector: 'mca-mini-cart',
  templateUrl: './mini-cart.component.html',
  styleUrls: ['./mini-cart.component.scss']
})
export class MiniCartComponent implements OnInit {

  @Input('cart-items')
  cartItems: CartItem[]

  constructor(public cartService: CartService) {
    this.cartItems = [];
   }

  ngOnInit(): void {
  
  }
  // applyCoupon(discountedItems: Partial<CartItem>[]) {
    // for(let discountedItem of discountedItems){
    //     let index = this.cartItems.map(cartItem => cartItem.id).indexOf(discountedItem.id);
    //     if(index != -1) {
    //       this.cartItems[index] = {
    //         ... this.cartItems[index],
    //         ... {
    //           discount: discountedItem.discount
    //         }
    //       }
    //     }
    //   }
    // }
  }





