import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SinglePaymentTotalComponent } from './single-payment-total.component';

describe('SinglePaymentTotalComponent', () => {
  let component: SinglePaymentTotalComponent;
  let fixture: ComponentFixture<SinglePaymentTotalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SinglePaymentTotalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SinglePaymentTotalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
