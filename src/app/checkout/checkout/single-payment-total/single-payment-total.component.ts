import { Component, Input, OnInit } from '@angular/core';
import { Discount } from 'src/app/shared/entities/copoun';

@Component({
  selector: 'mca-single-payment-total',
  templateUrl: './single-payment-total.component.html',
  styleUrls: ['./single-payment-total.component.scss']
})
export class SinglePaymentTotalComponent implements OnInit {
  readonly defaultTextColor = "#333";

  @Input('price')
  price: string;

  @Input('label')
  label: string;

  @Input('discount-label')
  discountLabel: string;

  @Input('discount-price')
  discountPrice: string;

  @Input('text-color')
  textColor: string;

  @Input('grey-out')
  set grey(val: boolean){
    this.textColor = val ? "gray" : this.defaultTextColor;
  }

  @Input('text-class')
  textClass: string;

  constructor() {
    this.textColor = this.defaultTextColor;
    this.textClass = "x-large-font";
    this.discountPrice = this.price;
  }

  ngOnInit(): void {
  }

  getTotal(){
    return this.hasDiscount() ? this.discountPrice : this.price;
  }

  hasDiscount(){
    return Boolean(this.discountPrice) && this.discountPrice != this.price;
  }

}
