import { Component, Inject, Input, OnInit } from '@angular/core';
import { Coupon } from 'src/app/shared/entities/copoun';
import { Price } from 'src/app/shared/entities/price';

@Component({
  selector: 'mca-payment-total',
  templateUrl: './payment-total.component.html',
  styleUrls: ['./payment-total.component.scss']
})
export class PaymentTotalComponent implements OnInit {

  @Input('show-totals')
  showTotal: boolean;

  @Input('shipping-cost')
  set shippingCost(val: Price){
    if(!val) {
      return;
    }

    this.showTotal = true;
    this._shippingCost = val;
    this.updateTotal();
  }

  @Input('item-cost')
  set itemCost(val: Price){
    this._itemCost = val;
  }

  get itemCost(){
    return this._itemCost;
  }

  get shippingCost() {
    return this._shippingCost;
  }

  @Input('discount-code')
  set discountCode(val: Coupon){
    this._discountCode = val;
  }

  get discountCode() {
    return this._discountCode;
  }

  _discountCode: Coupon;

  _itemCost: Price;
  _shippingCost: Price;

  total: Price;
  constructor(@Inject('CHECKOUT') public checkout) {
    this.showTotal = false;
    this.total = {
      subtotal: "0",
      total: "0"
    }
  }

  ngOnInit(): void {
  }

  updateTotal() {
    const total = ((+this.itemCost.total) + (+this.shippingCost.total)).toFixed(2);
    const subtotal = ((+this.itemCost.subtotal) + (+this.shippingCost.subtotal)).toFixed(2);
    this.total = {
      total: total,
      subtotal:subtotal
    }
  }


}
