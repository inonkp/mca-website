import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, FormGroup, Validators } from '@angular/forms';
import { MatRadioChange } from '@angular/material/radio';
import { ActivatedRoute } from '@angular/router';
import { DataServiceError, EntityCollectionService, EntityServices } from '@ngrx/data';
import { CartItem } from 'src/app/shared/entities/cart-item';
import { ShippingItem } from 'src/app/shared/entities/shipping-item';
import { ModelService } from 'src/app/shared/services/model.service';
import Catch from 'catch-finally-decorator';
import { Coupon } from 'src/app/shared/entities/copoun';
import { CartService } from '../../services/cart.service';
import { map, take, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Price } from 'src/app/shared/entities/price';

@Component({
  selector: 'mca-shipping-form',
  templateUrl: './shipping-form.component.html',
  styleUrls: ['./shipping-form.component.scss']
})
export class ShippingFormComponent implements OnInit {
  readonly maxShipping = "10000";

  set countryCode(code: string){
    this.getShippingItems(this.weight, code);
  }

  shippingFormControl: UntypedFormControl;

  loading: boolean;
  default: ShippingItem;
  cost$: Observable<Price>;
  @Input('shipping-items')
  set shippingItems(items: ShippingItem[]){
    this._shippingItems = items;
    this.default = this._shippingItems[0];
    this.shippingFormControl.setValue(this.default.id);
    // this.shippingFormControl = this.fb.control(this.default.id, Validators.required);
  }

  get shippingItems(){
    return this._shippingItems;
  }

  _shippingItems: ShippingItem[];

  @Input('disable')
  set disabled(val: boolean){
    if(val){
      this.shippingFormControl.disable({emitEvent: false});
    }else{
      this.shippingFormControl.enable({emitEvent: false});
    }
  }

  weight: number;
  shippingService: EntityCollectionService<ShippingItem>;

  constructor(entityServices: EntityServices,
     private cartService: CartService,
     @Inject('CHECKOUT') checkout) {
    this.shippingFormControl = new UntypedFormControl("");
    this.loading = false;
    this.shippingService = entityServices.getEntityCollectionService<ShippingItem>('ShippingItem');
    checkout.registerLoader(this.shippingService.loading$);
    this._shippingItems = [];
    // this.shippingItems = activatedRoute.snapshot.data.data.shipping;
    this.initWeight();
    this.cost$ = this.shippingFormControl.valueChanges.pipe(
      map(item => 
      {
         const shippingItem = this.getShippingItemById(item);
         return shippingItem.price;
       }
     )
   );

  }

  async initWeight() {
    this.weight = +await this.cartService.weight$.pipe(take(1)).toPromise();
  }

  ngOnInit(): void {

  }

  private getShippingItemById(id: string) {
    let shippingItem = this.shippingItems.find(shippingItem => shippingItem.id == id)
    return shippingItem;
  }

  getShippingItem(): ShippingItem {
    let itemId = this.shippingFormControl.value;
    return this.getShippingItemById(itemId);
  }

  hasItems(){
    return this.shippingItems.length > 0;
  }

  @Catch(DataServiceError, (err: DataServiceError, ctx: ShippingFormComponent) => {},
  (ctx: ShippingFormComponent) => {
    ctx.loading = false;
  })
  async getShippingItems(weight: number, countryCode: string){
    this.loading = true;
    this.shippingItems = await this.shippingService.getWithQuery({"weight": weight.toString(), 
      "country_code": countryCode, coupon: this.cartService.coupon}).toPromise();
  }

}
