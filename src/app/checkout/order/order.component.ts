import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { DataServiceError, EntityCollectionService, EntityServices } from '@ngrx/data';
import { take } from 'rxjs/operators';
import { ProductsService } from 'src/app/catalog/services/products.service';
import { CartItem } from 'src/app/shared/entities/cart-item';
import { MCAOrder } from 'src/app/shared/entities/order';
import { Product } from 'src/app/shared/entities/product';
import { CartService } from '../services/cart.service';
import Catch from 'catch-finally-decorator';
import { HttpErrorResponse } from '@angular/common/http';
import { McaSeoService } from 'src/app/shared/services/mca-seo.service';
import { Coupon } from 'src/app/shared/entities/copoun';
import { ShippingItem } from 'src/app/shared/entities/shipping-item';

@Component({
  selector: 'mca-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  get price() {
    return this.order.price;
  }

  orderService: EntityCollectionService<MCAOrder>;
  order: MCAOrder;
  cartItems: CartItem[];
  loaded: boolean;
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private seo: McaSeoService,
    private cartService: CartService) {
      seo.noindex()
      seo.setTitle("Order");
      this.loaded = false;
      if(activatedRoute.snapshot.data.data==null){
        this.productNotFound();
      }else{
        this.order = activatedRoute.snapshot.data.data.order;
        this.cartItems = activatedRoute.snapshot.data.data.cartItems;
      }
    }

  ngOnInit(): void {

  }

  productNotFound(){
    this.router.navigate(["/page-not-found"]);
  }



  getTotal() {
    // let shippingTotal = this.order.shipping.discount?.price ?? this.order.shipping.total_shipping_cost;
    // return ((+shippingTotal) + (+this.cartService.getCartTotal(this.cartItems))).toFixed(2)
  }

  getPreSaleTotal(){
    // return ((+this.order.shipping.total_shipping_cost) + (+this.cartService.getPreSaleTotal(this.cartItems))).toFixed(2)
  }
}

