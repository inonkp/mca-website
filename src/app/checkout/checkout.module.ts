import { NgModule } from '@angular/core';
import { CommonModule, Location } from '@angular/common';
import { CartComponent } from './cart/cart.component';
import { CartIconComponent } from './cart-icon/cart-icon.component';
import { SharedModule } from '../shared/shared.module';
import { CartItemRowComponent } from './cart/cart-item-row/cart-item-row.component';
import { CartQuantityComponent } from './cart/cart-quantity/cart-quantity.component';
import { CartItemTitleComponent } from './cart/cart-item-row/cart-item-title/cart-item-title.component';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { CheckoutComponent } from './checkout/checkout.component';
import { CheckoutButtonComponent } from './checkout/checkout-button/checkout-button.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { AddressFormComponent } from './checkout/address-form/address-form.component';
import { MiniCartComponent } from './checkout/mini-cart/mini-cart.component';
import { MiniCartItemComponent } from './checkout/mini-cart/mini-cart-item/mini-cart-item.component';
import { ShippingFormComponent } from './checkout/shipping-form/shipping-form.component';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { PaypalButtonComponent } from './checkout/paypal-button/paypal-button.component';
import { OrderComponent } from './order/order.component';
import { EmptyCartComponent } from './cart/empty-cart/empty-cart.component';
import { DiscountCodeComponent } from './checkout/discount-code/discount-code.component';
import { PaymentTotalComponent } from './checkout/payment-total/payment-total.component';
import { SinglePaymentTotalComponent } from './checkout/single-payment-total/single-payment-total.component';
import { EntityCollectionReducerMethodsFactory, PersistenceResultHandler } from '@ngrx/data';
import { CartPersistenceResultHandler } from './services/ngrx/cart-persistence-result-handler';
import { CartItemEntityCollectionReducerMethodsFactory } from './services/ngrx/cart-item-entity-collection-reducer-methods-factory';

@NgModule({
  declarations: [CartComponent, CartIconComponent, CartItemRowComponent, CartQuantityComponent, CartItemTitleComponent, CheckoutComponent, CheckoutButtonComponent, AddressFormComponent, MiniCartComponent, MiniCartItemComponent, ShippingFormComponent, PaypalButtonComponent, OrderComponent, EmptyCartComponent, DiscountCodeComponent, PaymentTotalComponent, SinglePaymentTotalComponent],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule
  ],
  exports: [
    CartIconComponent,
    CartQuantityComponent,
    CheckoutButtonComponent
  ],
  providers: [
    { provide: PersistenceResultHandler, useClass: CartPersistenceResultHandler },
    {
      provide: EntityCollectionReducerMethodsFactory,
      useClass: CartItemEntityCollectionReducerMethodsFactory
    },
    {
      provide: "COUPON",
      useFactory: (route: ActivatedRoute)  => {
        const urlParams = new URLSearchParams(route.snapshot.queryParams);
        if(urlParams.has('coupon-code')) {
          return urlParams.get('coupon-code');
        }
        return null;
      },
      deps: [ActivatedRoute]
    }
  ]
})
export class CheckoutModule { }
