import { Component, OnInit } from '@angular/core';
import { McaSeoService } from '../shared/services/mca-seo.service';

@Component({
  selector: 'mca-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor(private seo: McaSeoService) {
    seo.index();
    seo.setCatalogLd('');
    seo.setAboutDescription();
    seo.setTitle('About Us');
   }

  ngOnInit(): void {
  }

}
