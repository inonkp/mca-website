import { Pipe, PipeTransform } from '@angular/core';
import { MCAOrder } from 'src/app/shared/entities/order';
import { OrderStatus } from './orders.component';

@Pipe({
  name: 'orders'
})
export class OrdersPipe implements PipeTransform {

  transform(value: MCAOrder[], param: {status: string, search: string}): MCAOrder[] {

    if(param == null ) {
      return value;
    }

    return value.filter(o => o.status == param?.status.toString() || param?.status == 'all')
      .filter(o => o?.['short-id'].includes(param.search)
        || o.address?.['first-name']?.includes(param.search)
          || o.address?.['last-name']?.includes(param.search));
  }

}
