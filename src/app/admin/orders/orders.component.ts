import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, UntypedFormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { interval, Observable } from 'rxjs';
import { debounce } from 'rxjs/operators';
import { MCAOrder } from 'src/app/shared/entities/order';

export const OrderStatus = [
  {
    id: 'processing',
    label: 'Processing'
  },
  {
    id: 'complete',
    label: 'Complete'
  },
  {
    id: 'cancelled',
    label: 'Cancelled'
  },
  {
    id: 'refunded',
    label: 'Refunded'
  }
];

@Component({
  selector: 'mca-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  orders: MCAOrder[];
  formGroup: UntypedFormGroup;
  orderFilter$: Observable<any>;
  constructor(private activatedRoute: ActivatedRoute, fb: FormBuilder) {
    if(activatedRoute.snapshot.data.data==null){

    }else{
      this.orders = activatedRoute.snapshot.data.data;
    }
    const group = {
      search: '',
      status: 'all'
    };
    this.formGroup = fb.group(group);
    this.orderFilter$ = this.formGroup.valueChanges.
      pipe(debounce(() => interval(500)))
   }

  ngOnInit(): void {
  }

}
