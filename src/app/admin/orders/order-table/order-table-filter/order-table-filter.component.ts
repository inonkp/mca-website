import { Component, OnInit } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { OrderStatus } from '../../orders.component';

@Component({
  selector: 'mca-order-table-filter',
  templateUrl: './order-table-filter.component.html',
  styleUrls: ['./order-table-filter.component.scss']
})
export class OrderTableFilterComponent implements OnInit {

  orderStatus = OrderStatus;
  constructor(public formGroupDir: FormGroupDirective) { }

  ngOnInit(): void {
  }

}
