import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderTableFilterComponent } from './order-table-filter.component';

describe('OrderTableFilterComponent', () => {
  let component: OrderTableFilterComponent;
  let fixture: ComponentFixture<OrderTableFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderTableFilterComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrderTableFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
