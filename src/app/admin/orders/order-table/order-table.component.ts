import { Component, Input, OnInit } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { MCAOrder } from 'src/app/shared/entities/order';

@Component({
  selector: 'mca-order-table',
  templateUrl: './order-table.component.html',
  styleUrls: ['./order-table.component.scss']
})
export class OrderTableComponent implements OnInit {

  @Input('orders')
  orders: MCAOrder[];

  readonly displayedColumns = [
    'checkbox',
    'id',
    'name',
    'cost',
    'status',
    'date'
  ]
  isAllChecked: boolean;

  constructor() {
    this.isAllChecked = false;
   }

  ngOnInit(): void {
  }

}
