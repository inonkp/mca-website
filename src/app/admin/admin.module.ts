import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { LoginComponent } from './login/login.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JWTInterceptor } from './services/jwt-interceptor';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import { EtsyComponent } from './etsy/etsy.component';
import { OrdersComponent } from './orders/orders.component';
import { EtsyTableComponent } from './etsy/etsy-table/etsy-table.component';
import {MatTableModule} from '@angular/material/table';
import { EtsyItemTitleComponent } from './etsy/etsy-table/etsy-item-title/etsy-item-title.component';
import { AdminTableFilterComponent } from './admin-table-filter/admin-table-filter.component';
import { MatButtonModule } from '@angular/material/button';

import { OrderTableComponent } from './orders/order-table/order-table.component';
import { OrderTableFilterComponent } from './orders/order-table/order-table-filter/order-table-filter.component';
import { MatSelectModule } from '@angular/material/select';
import { OrdersPipe } from './orders/orders.pipe';
import { SalesComponent } from './sales/sales.component';
import { OrderComponent } from './orders/order/order.component';

@NgModule({
  declarations: [
    LoginComponent,
    AdminHomeComponent,
    EtsyComponent,
    OrdersComponent,
    EtsyTableComponent,
    EtsyItemTitleComponent,
    AdminTableFilterComponent,
    OrderTableComponent,
    OrderTableFilterComponent,
    OrdersPipe,
    SalesComponent,
    OrderComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSidenavModule,
    MatListModule,
    MatTableModule,
    FormsModule,
    MatButtonModule,
    MatSelectModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JWTInterceptor, multi: true },
  ]
})
export class AdminModule { }
