import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/catalog/services/products.service';
import { Product } from 'src/app/shared/entities/product';

@Component({
  selector: 'mca-etsy',
  templateUrl: './etsy.component.html',
  styleUrls: ['./etsy.component.scss']
})
export class EtsyComponent implements OnInit {

  products: Partial<Product>[];
  constructor(private productService: ProductsService) { }

  ngOnInit(): void {
    this.init();
  }

  async init() {
    let query = this.productService.getProductsQuery(0, 15);
    this.productService.addSelectQuery(query, ['slug', 'title', 'catalog_img', 'SKU']);
    this.products = await this.productService.getProducts(query);
  }

}
