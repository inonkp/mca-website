import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtsyItemTitleComponent } from './etsy-item-title.component';

describe('EtsyItemTitleComponent', () => {
  let component: EtsyItemTitleComponent;
  let fixture: ComponentFixture<EtsyItemTitleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtsyItemTitleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EtsyItemTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
