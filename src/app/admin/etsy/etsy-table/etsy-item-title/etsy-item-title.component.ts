import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/shared/entities/product';
import { ImageResizerService } from 'src/app/shared/services/image-resizer.service';

@Component({
  selector: 'mca-etsy-item-title',
  templateUrl: './etsy-item-title.component.html',
  styleUrls: ['./etsy-item-title.component.scss']
})
export class EtsyItemTitleComponent implements OnInit {


  @Input('product')
  product: Partial<Product>;

  thumbnail: string;
  title: string;
  fullImage: string;

  constructor() {

   }

  ngOnInit(): void {
    this.initThumbnail();
    this.initTitle();

  }

  initThumbnail(){
    this.thumbnail = this.product.catalog_img;
  }

  initTitle(){
    this.title = this.product.title;
  }


}
