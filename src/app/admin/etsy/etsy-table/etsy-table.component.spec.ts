import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtsyTableComponent } from './etsy-table.component';

describe('EtsyTableComponent', () => {
  let component: EtsyTableComponent;
  let fixture: ComponentFixture<EtsyTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtsyTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EtsyTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
