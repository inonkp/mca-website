import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/shared/entities/product';

@Component({
  selector: 'mca-etsy-table',
  templateUrl: './etsy-table.component.html',
  styleUrls: ['./etsy-table.component.scss']
})
export class EtsyTableComponent implements OnInit {

  @Input('products')
  products: Partial<Product>[]

  readonly displayedColumns = [
    'checkbox',
    'title',
    'sku',
    'status'
  ]
  isAllChecked: boolean;

  constructor() {
    this.isAllChecked = false;
   }

  ngOnInit(): void {
  }

}
