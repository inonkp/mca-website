import { Component, Input, OnInit } from '@angular/core';
import { FormGroupDirective, UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';

@Component({
  selector: 'mca-admin-table-filter',
  templateUrl: './admin-table-filter.component.html',
  styleUrls: ['./admin-table-filter.component.scss']
})
export class AdminTableFilterComponent implements OnInit {

  constructor(public formGroupDir: FormGroupDirective) {


  }

  ngOnInit(): void {

  }

}
