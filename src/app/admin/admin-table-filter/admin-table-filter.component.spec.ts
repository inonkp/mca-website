import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductTableFilterComponent } from './admin-table-filter.component';

describe('ProductTableFilterComponent', () => {
  let component: ProductTableFilterComponent;
  let fixture: ComponentFixture<ProductTableFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductTableFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductTableFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
