import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'mca-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formGroup: UntypedFormGroup;
  constructor(fb: UntypedFormBuilder, private authService : AuthService, private router : Router) {
    this.formGroup = fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
   }

  ngOnInit(): void {
  }

  async login(){
    let username = this.formGroup.get('username').value;
    let password = this.formGroup.get('password').value;

    let response = await this.authService.validate(username, password)

    this.authService.setToken(response['token']);
    this.router.navigate(['admin']);
  }

}
