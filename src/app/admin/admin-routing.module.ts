import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrderResolverService } from '../checkout/services/order-resolver.service';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { EtsyComponent } from './etsy/etsy.component';
import { LoginComponent } from './login/login.component';
import { OrdersComponent } from './orders/orders.component';
import { SalesComponent } from './sales/sales.component';
import { AuthGuardService } from './services/auth-guard.service';
import { OrdersResolverService } from './services/orders-resolver.service';

const routes: Routes = [
  { path: '', component: AdminHomeComponent, canActivate: [AuthGuardService],
    children: [
      {
        path: "etsy",
        component: EtsyComponent,
        outlet: "admin"
      },
      {
        path: "orders",
        component: OrdersComponent,
        outlet: "admin",
        resolve: {data: OrdersResolverService}
      },
      {
        path: "sales",
        component: SalesComponent,
        outlet: "admin"
      }
    ]
  },
  { path: 'login', component: LoginComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
