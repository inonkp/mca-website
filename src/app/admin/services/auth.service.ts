import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http : HttpClient) { }

  public async isAuthenticated() {
    const token =  this.getToken();
    if(token === null) {
      throw  new HttpErrorResponse({});
    }
    await this.http.get(environment.api + "authenticate/?token=" + token).toPromise();
    return true;
  }

  public setToken(token){
    localStorage.setItem("jwt-token", token);
  }

  getToken() {
    return localStorage.getItem("jwt-token") ?? null;
  }

  public validate(email: string, password: string) {
    return this.http.post(environment.api + "authenticate", {'username' : email, 'password' : password}).toPromise()
  }
}
