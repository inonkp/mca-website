import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

/** Pass untouched request through to the next request handler. */
@Injectable()
export class JWTInterceptor implements HttpInterceptor {
  constructor() {

  }
  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {
      const token = localStorage.getItem("jwt-token") ?? "";
      req.headers.append('Authorization', "Bearer " + token);
    return next.handle(req).pipe(tap(
      e => {
        if(e instanceof HttpResponse) {
          const token = e.headers.get('Authorization');
          localStorage.setItem("jwt-token", token);
        }
      }
    ));
  }
}
