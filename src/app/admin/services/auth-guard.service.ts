import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';
import Catch from 'catch-finally-decorator';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  public static readonly tokenKey = "jwt-token";

  constructor(private authService : AuthService, private route : Router) { }

  @Catch(HttpErrorResponse, (err, ctx: AuthGuardService) => ctx.routeToLogin())
  async canActivate(){
    let res = await this.authService.isAuthenticated();
    return res;
  }

  routeToLogin(){
    this.route.navigate(['admin/login']);
    return false;
  }
}
