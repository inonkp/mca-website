import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { MCAOrder } from 'src/app/shared/entities/order';
import Catch from 'catch-finally-decorator';
import { DataServiceError, EntityCollectionService, EntityServices } from '@ngrx/data';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OrdersResolverService implements Resolve<MCAOrder[]> {
  orderService: EntityCollectionService<MCAOrder>;

  constructor(private entityServices: EntityServices) {
    this.orderService = entityServices.getEntityCollectionService<MCAOrder>("MCAOrder");
  }

  @Catch(DataServiceError, (err: DataServiceError, ctx: OrdersResolverService) => {

  })
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<MCAOrder[]> {
    let orders = this.orderService.getWithQuery({
      skip: "0",
      limit: "20",
      sort: "-createdAt"
    }).toPromise();

    return orders;
  }
}
