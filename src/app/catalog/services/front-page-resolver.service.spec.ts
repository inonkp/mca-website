import { TestBed } from '@angular/core/testing';

import { FrontPageResolverService } from './front-page-resolver.service';

describe('FrontPageResolverService', () => {
  let service: FrontPageResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FrontPageResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
