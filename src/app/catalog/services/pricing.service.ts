import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpUrlGenerator, QueryParams } from '@ngrx/data';
import { ModelItem } from 'src/app/shared/entities/model-item';
import { Price } from 'src/app/shared/entities/price';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PricingService {

    private productPricingApi: string;
    private checkoutTotalApi: string;
    loading: boolean;
    constructor(httpUrlGenerator: HttpUrlGenerator, private http: HttpClient) {
      this.productPricingApi = httpUrlGenerator.entityResource('Product', environment.api, true) + 'price/';
      this.checkoutTotalApi = environment.api + "/checkout/total"
      this.loading = false;
    }

    async getProductPrice(pid: string, modelId: string, quantity: number | string): Promise<Price> {

      const api = new URL(this.productPricingApi + pid);
      api.searchParams.append("model_id", modelId);
      api.searchParams.append("quantity", quantity.toString());
      this.loading = true;
      const price = await this.http.get(api.href).toPromise() as Price;
      this.loading = false;
      return price;
    }

    async getCheckoutTotal(shipping: Price, cartTotal: Price) {
      const api = new URL(this.checkoutTotalApi);
      api.searchParams.append('cart_total', cartTotal.total)
      api.searchParams.append('cart_subtotal', cartTotal.subtotal)
      api.searchParams.append('shipping_total', shipping.total);
      api.searchParams.append('shipping_subtotal', shipping.subtotal);
      this.loading = true;
      const price = await this.http.get(api.href).toPromise() as Price;
      this.loading = false;
      return price;
      
    }
  }
