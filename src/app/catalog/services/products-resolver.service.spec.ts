import { TestBed } from '@angular/core/testing';

import { ProducsResolverService } from './products-resolver.service';

describe('ProducsResolverService', () => {
  let service: ProducsResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProducsResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
