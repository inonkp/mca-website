import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Product } from 'src/app/shared/entities/product';
import { ProductsService } from './products.service';
import Catch from 'catch-finally-decorator';
import { DataServiceError } from '@ngrx/data';

@Injectable({
  providedIn: 'root'
})
export class ProductResolverService implements Resolve<Product> {

  constructor(private productService: ProductsService) { }

  @Catch(DataServiceError, (err: DataServiceError, ctx: ProductResolverService) => {
    return of(ctx.productService.placeHolderProduct);
  })
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Product | Observable<Product> | Promise<Product> {
    return this.productService.getProduct(route.params.slug);
  }
}
