import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { HttpUrlGenerator } from '@ngrx/data';
import { Observable } from 'rxjs';
import { Product } from 'src/app/shared/entities/product';
import { environment } from 'src/environments/environment';
import { FrontPageComponent } from '../front-page/front-page.component';
import { ProductsService } from './products.service';

@Injectable({
  providedIn: 'root'
})
export class FrontPageResolverService implements Resolve<Product[]>{

  salesApi: string;
  constructor(httpUrlGenerator: HttpUrlGenerator, private http: HttpClient) {
    this.salesApi = httpUrlGenerator.entityResource('Product', environment.api, true) + 'sales/active';
   }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Product[] | Observable<Product[]> | Promise<Product[]> {
    return this.http.get(this.salesApi, {}) as Observable<Product[]>;
  }
}
