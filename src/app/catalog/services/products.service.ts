import { Injectable } from '@angular/core';
import { EntityCollectionDataService, EntityDataService, QueryParams } from '@ngrx/data';
import { MCAImage } from 'src/app/shared/entities/mca-image';
import { Product } from 'src/app/shared/entities/product';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  get placeHolderId(){
    return "";
  }

  get placeHolderProduct(): Product{
    return {
      id: this.placeHolderId,
      title: "",
      slug: "",
      description: "",
      templates: [],
      catalog_img: "/assets/images/placeholder-image.png",
      images_json: []
    }
  }

  productsService: EntityCollectionDataService<Product>;
  imageService: EntityCollectionDataService<MCAImage>;
  constructor(entityDataService: EntityDataService) {
    this.productsService = entityDataService.getService('Product');
    this.imageService = entityDataService.getService('MCAImage');
  }

  public getTagsQuery(tag: string): QueryParams {
    return {
      filter: JSON.stringify({tags: {$elemMatch:  {$eq: tag}}})
    };
  }

  public getCategoryQuery(category: string): QueryParams{
    return {
      filter: JSON.stringify({categories: {$elemMatch:  {$eq: category}}})
    };
  }

  public getSearchQuery(search: string): any {
    let orQuery = {
      $or: [
        {tags: {$elemMatch:  {$eq: search}}},
        {title : {$regex : `(?i).*${search}.*`}}
      ]
    }
    return {
      filter: JSON.stringify(orQuery)
    }
  }

  public getProductsQuery(start: number, limit: number, sort = true): QueryParams {
    let query =  {
      skip: start.toString(),
      limit: limit.toString()
    };
    if(sort){
      query['sort'] = "-likes,-_id";
    }
    return query;
  }

  public addSelectQuery(query: QueryParams, fields: string[]){
    query['select'] = fields.join(",");
  }

  public getProducts(query: QueryParams): Promise<Product[]>{
    return this.productsService.getWithQuery(query).toPromise();
  }

  public getProduct(slug: string): Promise<Product>{
    return this.productsService.getById(slug).toPromise();
  }

  public async getProductsByIds(prodIds: string[]){
    let query= { _id: { $in: prodIds } };
    return this.productsService.getWithQuery({filter: JSON.stringify(query)}).toPromise();
  }
}
