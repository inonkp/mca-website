import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Product } from 'src/app/shared/entities/product';
import { CatalogComponent } from '../catalog/catalog.component';
import { ProductsService } from './products.service';

@Injectable({
  providedIn: 'root'
})
export class ProductsResolverService implements Resolve<Product[]>  {

  constructor(private productService: ProductsService) { }
  resolve(activatedRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Product[] | Observable<Product[]> | Promise<Product[]> {
    let data = activatedRoute.data;
    let paramMap = activatedRoute.paramMap;
    let param = paramMap.get(data.catalogParam)
    let queryParams = data.query(param);
    let pageNum = activatedRoute.queryParamMap.get('total_page_num') ?? 1;
    let productsQuery = this.productService.getProductsQuery(0,CatalogComponent.PAGE_SIZE * Number(pageNum));
    return this.productService.getProducts({...queryParams, ...productsQuery, select: CatalogComponent.SELECT_QUERY});
  }
}
