import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DeviceDetectorService } from 'ngx-device-detector';
import { take } from 'rxjs/operators';
import { ImageResizerService } from 'src/app/shared/services/image-resizer.service';
import { McaSeoService } from 'src/app/shared/services/mca-seo.service';
import { FullImageDialogComponent } from '../../../shared/components/full-image-dialog/full-image-dialog.component';

@Component({
  selector: 'mca-gallery-images',
  templateUrl: './gallery-images.component.html',
  styleUrls: ['./gallery-images.component.scss']
})
export class GalleryImagesComponent implements OnInit, OnDestroy {

  @Input('template')
  set template(template: string) {
    if(!template) return;
    
    const selectedImages = this.imagesJson[template];
    selectedImages.forEach((img) => {
      if (img.type === 'main') {
        this.changeImg = img;
      }
    });
  }

  @Input('image-preview')
  set changeImg(img: any){
    this.mainImage = img;
  }

  imagesJson: any;

  @Input('images')
  set images_json(val){
    this.imagesJson = val;
    this.images = [];
    this.galleryImages = [];
    Object.keys(val).forEach(template => this.images = this.images.concat(val[template]));
    this.images.forEach(image => {
      try{
        this.galleryImages.push(image);
      }catch(err){
        // this error may occur on server side rendering where there is no Image
        // so we catch it without returning from the method
      }


    })
  };

  @Input('alt-text')
  set altText(val: string){
    this._altText = this.seo.formatAlt(val);
  }
  _altText: string;

  get altText(){
    return this._altText;
  }

  images: any[];
  galleryImages = [];
  mainImage: any;
  constructor(private imageResizer: ImageResizerService, private seo: McaSeoService) {
    }

  ngOnInit(): void {
  }

  public openLargeImgModal(img: any): void {

    this.imageResizer.openLargeImgModal(img.full);
  }


  ngOnDestroy(){

  }

}
