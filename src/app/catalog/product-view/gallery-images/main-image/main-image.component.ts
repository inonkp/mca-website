import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { Product } from 'src/app/shared/entities/product';
import { ImageResizerService } from 'src/app/shared/services/image-resizer.service';
import { McaSeoService } from 'src/app/shared/services/mca-seo.service';
import { MediaService } from 'src/app/shared/services/media.service';
import { device } from 'src/environments/device';

@Component({
  selector: 'mca-main-image',
  templateUrl: './main-image.component.html',
  styleUrls: ['./main-image.component.scss']
})
export class MainImageComponent implements OnInit {

  @Output('image-click')
  clickEmitter = new EventEmitter<void>();
  
  get desktop() {
    if(!this.image.sizes.medium_large){
      // this is a falback if there is no medium_large
      this.image.sizes.medium_large = this.getImageResized(this.image, 760, 500);
    }
    return this.image.sizes.medium_large;
  }

  get mobile() {
    if(!this.image.sizes.shop_single){
      // this is a falback if there is no medium_large
      this.image.sizes.shop_single = this.getImageResized(this.image, 416, 416);
    }
    return this.image.sizes.shop_single;
  }

  get src() {
    return (device.mobile && this.media.lg) || this.media.lg ? this.desktop : this.mobile
  }

  get alt() {
    return this.seo.formatAlt(this.product.title);
  }

  @Input('image')
  image: any;

  constructor(private seo: McaSeoService, public imageResizer: ImageResizerService,
      @Inject("PRODUCT") private product: Product, private media: MediaService){ }

  ngOnInit(): void {
  }

  getImageResized(img: any, width: number, height: number){
    let full = img.full;
    return this.imageResizer.getResized(full, width, height);
  }
}
