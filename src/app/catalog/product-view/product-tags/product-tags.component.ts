import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'mca-product-tags',
  templateUrl: './product-tags.component.html',
  styleUrls: ['./product-tags.component.scss']
})
export class ProductTagsComponent implements OnInit {

  @Input('tags')
  tags: string[];

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  getByProductTagName(tag: string){
    this.router.navigate(["/tags/" + tag]);
  }

}
