import { Component, Inject, Input, OnInit } from '@angular/core';
import { CartService } from 'src/app/checkout/services/cart.service';
import { CartItem, CartItemStatus } from 'src/app/shared/entities/cart-item';
import { ProductOption } from '../product-options/product-option';
import { faPlus } from "@fortawesome/free-solid-svg-icons/faPlus";
import { faMinus  } from "@fortawesome/free-solid-svg-icons/faMinus";

import { Product } from 'src/app/shared/entities/product';
import { ModelItem } from 'src/app/shared/entities/model-item';
import { PricingService } from '../../services/pricing.service';
import { ModelService } from 'src/app/shared/services/model.service';

@Component({
  selector: 'mca-cart-button',
  templateUrl: './cart-button.component.html',
  styleUrls: ['./cart-button.component.scss']
})
export class CartButtonComponent implements OnInit {

  get faMinus(){
    return faMinus;
  }

  get faPlus(){
    return faPlus;
  }

  @Input('model-id')
  modelId: string;

  productQuantity: number;
  addedToCart: boolean;
  constructor(private cartService: CartService, @Inject("PRODUCT") private product: Product,
    public pricingService: PricingService, private modelService: ModelService) {
    this.addedToCart = false;
    this.modelId = null;
  }

  ngOnInit(): void {
    this.productQuantity = 1;

  }

  public subtractFromQuantity(): void {
    if (this.productQuantity <= 1) {
      return;
    } else {
      this.productQuantity -= 1;
    }
  }

  public addToQuantity(): void {
    this.productQuantity += 1;
  }

  async addProductToCart(): Promise<void> {
    const model = this.modelService.getModelItemById(this.modelId);
    // initializing the cart full and thumnail images
    let template = model.template;
    let thumbnail = this.product.catalog_imgs[template];
    let full = thumbnail;
    let imageArr = this.product.images_json[template];
    if(imageArr){
      imageArr.forEach((img) => {
        if (img?.type === 'main') {
          full = img.full ?? full;
          thumbnail = img?.sizes?.shop_thumbnail ?? thumbnail;
        }
      });
    }


    const cartItem: CartItem = {
      modelId: this.modelId,
      product: {
        "title": this.product.title,
        "slug": this.product.slug,
        id: this.product.id,
        catalog_img: thumbnail,
        image: full
      },
      quantity: this.productQuantity,
      status: CartItemStatus.PENDING,
      price: null
    };
    await this.cartService.add(cartItem);
    this.addedToCart = true;
  }

}
