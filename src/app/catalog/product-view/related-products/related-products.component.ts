import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/shared/entities/product';
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'mca-related-products',
  templateUrl: './related-products.component.html',
  styleUrls: ['./related-products.component.scss']
})
export class RelatedProductsComponent implements OnInit {
  readonly PRODUCT_NUM = 8;
  relatedProducts: Product[];
  @Input('product')
  set product(p: Product){
    this._product = p;
    this.getRelatedProducts();
  }

  _product: Product;


  constructor(private productsService: ProductsService) {

   }

  ngOnInit(): void {
  }

  async getRelatedProducts(){
    const params = {
      ... this.productsService.getProductsQuery(0, this.PRODUCT_NUM),
      ... this.productsService.getCategoryQuery(this._product?.categories[0])
  };
  const getRelatedProducts: Product[] = await this.productsService.getProducts(params);
  this.relatedProducts = getRelatedProducts;
  }

}
