import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { DataEntity } from 'src/app/shared/entities/data-entitiy';
import { ProductOption } from './product-option';
import { Framing, ModelItem, Size, Template } from 'src/app/shared/entities/model-item';
import { MatSelectChange } from '@angular/material/select';
import { ModelService } from 'src/app/shared/services/model.service';
import { Price } from 'src/app/shared/entities/price';
import { FormGroup } from '@angular/forms';
import { Product } from 'src/app/shared/entities/product';

type SizeEntity = {
  id: Size;
  name: string;
}

type FramingEntity = {
  id: Framing;
  name: string;
}

type TemplateEntity = {
  id: Template;
  name: string;
}

@Component({
  selector: 'mca-product-options',
  templateUrl: './product-options.component.html',
  styleUrls: ['./product-options.component.scss']
})
export class ProductOptionsComponent implements OnInit {
  productModelValues: ModelItem[];
  optionModel: ProductOption;

  framing: FramingEntity[] = [
    {
      id: "F",
      name: "Framed"
    },
    {
      id: "PO",
      name: "Posters Only"
    }
  ]

  sizes: SizeEntity[] = [
    {
      id: "S",
      name: "Small"
    },
    {
      id: "M",
      name: "Medium"
    },
    {
      id: "L",
      name: "Large"
    }
  ]

  templatesModel: TemplateEntity[] = [
    {
      id: "H1",
      name: "1 Panel"
    },
    {
      id: "H3",
      name: "3 Panels"
    },
    {
      id: "H4",
      name: "4 Panels"
    },
    {
      id: "H5",
      name: "5 Panels"
    }
  ]
  templates: DataEntity[] = [];
  currentOption: ProductOption;

  set productTemplates(productTemplates: string[]){
    this.templates = this.templatesModel.filter(template => productTemplates.includes(template.id));
  }

  @Input('form-group')
  formGroup: FormGroup;

  @Output("option-selected")
  optionEmitter: EventEmitter<ProductOption> = new EventEmitter();
  constructor(private modelService: ModelService, @Inject('PRODUCT') public product: Product) {
    this.productTemplates = product.templates;
   }

  ngOnInit(): void {
    this.productModelValues = this.modelService.model;
    this.currentOption = {};
  }

}
