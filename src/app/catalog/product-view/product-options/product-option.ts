import { Framing, Size, Template } from "src/app/shared/entities/model-item";

export interface ProductOption{
  size?: Size;
  framing?: Framing;
  template?: Template;
  description?: string;
  summary?: string;
  price?: string;
}
