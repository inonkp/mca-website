import { Component, Inject, Input, OnInit } from '@angular/core';
import { ModelItem } from 'src/app/shared/entities/model-item';
import { Price } from 'src/app/shared/entities/price';
import { Product } from 'src/app/shared/entities/product';
import { PricingService } from '../../services/pricing.service';
import { faSpinner } from '@fortawesome/free-solid-svg-icons/faSpinner';
import { ModelService } from 'src/app/shared/services/model.service';

@Component({
  selector: 'mca-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {

  get faSpinner(){
    return faSpinner;
  }

  @Input('model-id')
  set modelId(val: string | null) {
    if(!val) return;
    this.modelItem = this.modelService.getModelItemById(val);
    this.updatePrice();
  }

  price: Price | null;

  modelItem: ModelItem | null;
  loading: boolean;
  constructor(@Inject("PRODUCT") private product: Product, public pricingService: PricingService,
    private modelService: ModelService) {
    this.price = null;
    this.modelItem = null; 
    this.loading = false;
  }

  ngOnInit(): void {
  }

  async updatePrice() {
    this.loading = true;
    this.price = await this.pricingService.getProductPrice(this.product.id, this.modelItem.id, 1);
    this.loading = false;
  }

}
