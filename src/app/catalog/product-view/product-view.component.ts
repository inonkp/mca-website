import {Component, Inject, OnDestroy, OnInit, Self} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {
  DataServiceError,
  EntityCollectionDataService,
  EntityDataService, HttpUrlGenerator
} from '@ngrx/data';
import {Product} from '../../shared/entities/product';
import { ProductOption } from './product-options/product-option';
import { ProductsService } from '../services/products.service';
import { last, skip, skipWhile, take, takeUntil, takeWhile } from 'rxjs/operators';
import {Subject} from 'rxjs';
import { CartItem } from 'src/app/shared/entities/cart-item';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {CartService} from '../../checkout/services/cart.service';
import { SeoSocialShareService  } from 'ngx-seo';
import { McaSeoService } from 'src/app/shared/services/mca-seo.service';

@Component({
  selector: 'mca-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.scss'],
  providers: [{provide: "PRODUCT", useFactory: 
    (activatedRoute: ActivatedRoute) => activatedRoute.snapshot.data.product, 
    deps: [ActivatedRoute]}]
})

export class ProductViewComponent implements OnInit, OnDestroy {

  selectedOption: ProductOption;
  isOptionSelected: boolean;
  private destroy$: Subject<void> = new Subject<void>();

  constructor(@Inject('PRODUCT') @Self() public product: Product,
              private productsService: ProductsService,
              private router: Router, private seo: McaSeoService) {
    
    if(product == null)
    {
      this.productNotFound();
      return;
    }
    this.product.templates = this.product.templates.sort((a,b) => b.localeCompare(a));
    this.seo.index();
    this.seo.setTitle(this.product.title);
    this.seo.setDescription(this.product.description);
    this.seo.setProductJsonLd(this.product);
  }

  ngOnInit(){

  }

  productNotFound(){
    this.router.navigate(["/page-not-found"]);
  }

  isPlaceHolder(){
    return this.product.id == this.productsService.placeHolderId;
  }


  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
