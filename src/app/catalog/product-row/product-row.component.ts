import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Product } from 'src/app/shared/entities/product';

@Component({
  selector: 'mca-product-row',
  templateUrl: './product-row.component.html',
  styleUrls: ['./product-row.component.scss']
})
export class ProductRowComponent implements OnInit {

  @Output('on-route')
  onProductRoute: EventEmitter<string>;

  @Input('row-size')
  rowSize: number = 4;

  get md(){
   return Math.min(100/this.rowSize, 40);
  }

  @Input('products')
  products: Product[];

  constructor() {
    this.onProductRoute = new EventEmitter();
    this.products = [];
   }

  ngOnInit(): void {
  }

}
