import { Component, Inject, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModelItem } from 'src/app/shared/entities/model-item';
import { Product } from 'src/app/shared/entities/product';
import { ModelService } from 'src/app/shared/services/model.service';

@Component({
  selector: 'mca-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit {

  get template() {
    return this.formGroup.get('template').value;
  }

  get modelId() {
    return this.formGroup.valid ? this.getModelItem(this.formGroup.value).id : null;
  }

  formGroup: FormGroup;
  constructor(@Inject('PRODUCT') public product: Product, fb: FormBuilder,
     private modelService: ModelService) { 
    this.formGroup = fb.group({
      'template': [null, Validators.required],
      'size': [null, Validators.required],
      'framing': [null, Validators.required]
    })
  }

  ngOnInit(): void {
  }

  getModelItem(val: Partial<ModelItem>){
    return this.modelService.getModelItem(val);
  }

}
