import {Component, EventEmitter, HostListener, OnInit, Output} from '@angular/core';
import {EntityCollectionDataService, EntityDataService} from '@ngrx/data';
import {Product} from 'src/app/shared/entities/product';
import {MCAImage} from 'src/app/shared/entities/mca-image';
import {default as CategoriesModel} from "src/assets/data/categories.json";
import { Category } from 'src/app/shared/entities/category';
import { CategoriesShuffler } from './categories-shuffle';

@Component({
  selector: 'mca-categories-feature',
  templateUrl: './categories-feature.component.html',
  styleUrls: ['./categories-feature.component.scss']
})
export class CateogriesFeatureComponent {

  readonly PULL_NUM = 2;
  latestFeature = 0;
  @HostListener('window:scroll', [])
  onScroll(): void {
    if (this.loadedCategories == this.model.length || this.loading) {
      return;
    }

    if ((window.innerHeight + window.scrollY) + 1 >= document.body.scrollHeight) {


      this.loadMoreFeatures();
    }
  }

  products: Product[] = [];
  model: Category[];
  categories: Category[] = []
  tags: string[] = [];
  loading = false;
  @Output('load')
  load: EventEmitter<void> = new EventEmitter();;

  loadedCategories: number = 0;
  /**
   * category title, see more button
   * row of tags (search words)
   * 3 larger images
   * 2 smaller images
   *
   * How to get products from specific category:
   * use contains parameter (https://strapi.io/documentation/developer-docs/latest/developer-resources/content-api/content-api.html#api-endpoints)
   * use star-wars, dragon-ball, dc, marvel
   */

  constructor(entityDataService: EntityDataService) {
    let shuffler = new CategoriesShuffler();
    this.model = CategoriesModel;
    this.products = [];
    this.loadMoreFeatures();
  }

  loadMoreFeatures(){
    if(this.latestFeature >= this.model.length) return;
    let slice = this.model.slice(this.latestFeature, this.latestFeature + this.PULL_NUM)
    this.categories = this.categories.concat(slice);
    this.latestFeature+=this.PULL_NUM;
    this.loading = true;
  }

  categoryLoaded(){
    this.loadedCategories++;
    if(this.loadedCategories == this.categories.length){
      this.load.emit();
      this.loading = false;
    }
  }
}
