import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CateogriesFeatureComponent } from './categories-feature.component';

describe('CatalogComponent', () => {
  let component: CateogriesFeatureComponent;
  let fixture: ComponentFixture<CateogriesFeatureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CateogriesFeatureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CateogriesFeatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
