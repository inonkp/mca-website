import { Category } from "src/app/shared/entities/category";

export class CategoriesShuffler{
  private readonly randomShuffleFn = () => Math.random() - .5;

  private readonly shuffleFn = (candidateA, candidateB) =>
    Math.random() * (candidateB.weight + candidateA.weight) - candidateA.weight;

    shuffle(cats: Category[]){
      return cats.sort(this.randomShuffleFn).sort(this.shuffleFn);
    }
}
