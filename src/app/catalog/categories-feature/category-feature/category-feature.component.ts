import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Product } from 'src/app/shared/entities/product';
import { Router } from '@angular/router';
import { DataServiceError } from '@ngrx/data';
import Catch from 'catch-finally-decorator';
import { MCAImage } from 'src/app/shared/entities/mca-image';
import { ProductsService } from '../../services/products.service';
import { Category } from 'src/app/shared/entities/category';

@Component({
  selector: 'mca-category-feature',
  templateUrl: './category-feature.component.html',
  styleUrls: ['./category-feature.component.scss']
})
export class CategoryFeatureComponent implements OnInit {

  readonly PRODUCT_NUM = 8;

  @Input('category')
  set category(val: Category) {
    this.mCategory = val;
    this.init(val);
  }

  @Output('load')
  load: EventEmitter<void> = new EventEmitter();

  get category(): Category {
    return this.mCategory;
  }
  mCategory: Category;

  @Input('featured')
  featured: boolean;

  products: Product[] = [];
  tags: string[] = [];

  constructor(private router: Router, private productsService: ProductsService) { }

  ngOnInit(): void {
  }

  public getAllProducts(categoryToShow: string): void {
    this.router.navigate(['categories', categoryToShow]);
  }


  async init(category: Category): Promise<any> {
    const params = {
      ... this.productsService.getProductsQuery(0, this.PRODUCT_NUM),
      ... this.productsService.getCategoryQuery(category.slug)
    }
    const categoryShowProducts = await this.productsService.getProducts(params);
    categoryShowProducts.forEach(product => {
      this.tags = [ ... new Set(this.tags.concat(product.tags))];
    });
    this.products = categoryShowProducts;
    this.load.emit();
  }
}
