import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Product } from 'src/app/shared/entities/product';
import { Router } from '@angular/router';
import { McaSeoService } from 'src/app/shared/services/mca-seo.service';
import { MediaService } from 'src/app/shared/services/media.service';
import { device } from 'src/environments/device';


@Component({
  selector: 'mca-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {

  @Output('on-route')
  onProductRoute: EventEmitter<string>;

  get altText(){
    return this._altText;
  }

  get catalog_img() {
    return (device.mobile && this.media.lg) || this.media.lg ? this.product.catalog_img : this.product.mbl_catalog_image;
  }

  _altText: string;

  @Input('product')
  product: Product;

  mImageLoaded: boolean = false;
  constructor(private router: Router, private seo: McaSeoService, private media: MediaService) {
    this.onProductRoute = new EventEmitter();
  }

  ngOnInit(): void {
    this._altText = this.seo.formatAlt(this.product.title);

  }

  public getProductView(productView): void {
    this.onProductRoute.emit(this.product.slug);
    // this.router.navigate(['product/' + productView.slug]);
  }

  imageLoaded(){
    this.mImageLoaded = true;
  }


}
