import { Component, Input, OnInit } from '@angular/core';
import { faHeart  } from "@fortawesome/free-solid-svg-icons/faHeart";
import { faHeart as faEmptyHeart } from "@fortawesome/free-regular-svg-icons/faHeart";
import { Product } from 'src/app/shared/entities/product';
import { DataServiceError, HttpUrlGenerator } from '@ngrx/data';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import Catch from 'catch-finally-decorator';
import { animate, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'mca-product-card-header',
  templateUrl: './product-card-header.component.html',
  styleUrls: ['./product-card-header.component.scss'],
  animations: [
    trigger('fadeIn', [
        transition(':enter', [   // :enter is alias to 'void => *'
        style({opacity:0.6}),
        animate(200, style({opacity:1}))
      ]),
    ])
  ]
})
export class ProductCardHeaderComponent implements OnInit {

  @Input('product')
  product: Product;

  get faHeart(){
    return faHeart;
  }

  get faEmptyHeart(){
    return faEmptyHeart;
  }

  loading: boolean;
  liked: boolean;
  likeNum: number;
  likeApi: string;
  constructor(httpUrlGenerator: HttpUrlGenerator, private http: HttpClient) {
    this.likeApi = httpUrlGenerator.entityResource('Product', environment.api, true);
    this.liked = false;
    this.likeNum = 0;
    this.loading = false;
   }

  ngOnInit(): void {
    let likes = this.product.likes ?? 0;
    this.updateLikeNum(likes);
  }

  updateLikeNum(likes: number){
    if(likes > 0){
      this.likeNum = likes;
    }
    if(likes > 10){
      this.likeNum = 10;
    }
    if(likes > 100){
      this.likeNum = 100;
    }
    if(likes > 1000){
      this.likeNum = 1000;
    }
  }

  showLikes(){
    return this.likeNum >= 10;
  }

  @Catch(DataServiceError, (err: DataServiceError, ctx: ProductCardHeaderComponent) => {},
    (ctx: ProductCardHeaderComponent) => {
      ctx.loading = false;
    })
  async like(){
    this.loading = true;
    this.liked = true;

    let ret = await this.http.put(this.likeApi + this.product.slug + "/like",{} ).toPromise();
    let likes = (ret as Product).likes;
    this.updateLikeNum(likes);
  }


}
