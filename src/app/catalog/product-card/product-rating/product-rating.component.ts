import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/shared/entities/product';
import { faStar } from "@fortawesome/free-solid-svg-icons/faStar";

@Component({
  selector: 'mca-product-rating',
  templateUrl: './product-rating.component.html',
  styleUrls: ['./product-rating.component.scss']
})
export class ProductRatingComponent implements OnInit {
  productRating: Array<number>;

  get faStar(){
    return faStar;
  }

  @Input('product')
  product: Product;

  constructor() {
    this.productRating = [];
   }

  ngOnInit(): void {
    let rating = this.product.rating ?? 0;
    for(let i=0; i< rating; i++){
      this.productRating.push(0);
    }
  }

}
