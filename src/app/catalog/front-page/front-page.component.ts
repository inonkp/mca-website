import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from 'src/app/shared/entities/product';
import { McaSeoService } from 'src/app/shared/services/mca-seo.service';

@Component({
  selector: 'mca-front-page',
  templateUrl: './front-page.component.html',
  styleUrls: ['./front-page.component.scss']
})
export class FrontPageComponent implements OnInit {
  loaded: boolean = false;
  onSale: Product[];
  constructor(activatedRoute: ActivatedRoute, private seo: McaSeoService) {
    this.onSale = activatedRoute.snapshot.data.products;
    seo.index();
    seo.setCatalogLd('');
    seo.setAboutDescription();
    seo.setTitle('Home');
  }

  ngOnInit(): void {
  }

//   shuffleArray(array: Array<any>) {
//     for (var i = array.length - 1; i > 0; i--) {
//         var j = Math.floor(Math.random() * (i + 1));
//         var temp = array[i];
//         array[i] = array[j];
//         array[j] = temp;
//     }
//     return array;
// }

}
