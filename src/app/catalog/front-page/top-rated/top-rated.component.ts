import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/shared/entities/product';
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'mca-top-rated',
  templateUrl: './top-rated.component.html',
  styleUrls: ['./top-rated.component.scss']
})
export class TopRatedComponent implements OnInit {

  topRatedProduct: Product[];
  constructor(private productService: ProductsService) {
    this.topRatedProduct = [];
    this.initFeaturesProducts();
   }

  ngOnInit(): void {
  }

  async initFeaturesProducts() {
    let mainQuery = this.productService.getProductsQuery(0,20);
    let featuredQuery = {
      rating: "5",
      ...mainQuery
    }
    this.topRatedProduct = await this.productService.getProducts(featuredQuery);
  }

}
