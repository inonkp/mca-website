import { Component, Input, OnInit } from '@angular/core';
import { faStar } from "@fortawesome/free-solid-svg-icons/faStar";

@Component({
  selector: 'mca-five-star',
  templateUrl: './five-star.component.html',
  styleUrls: ['./five-star.component.scss']
})
export class FiveStarComponent implements OnInit {

  get faStar(){
    return faStar;
  }

  @Input('subtitle')
  subtitle: string;

  stars: Array<any>
  constructor() {
    this.stars = Array(5);
  }

  ngOnInit(): void {


  }

}
