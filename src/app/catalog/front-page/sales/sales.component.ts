import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/shared/entities/product';

@Component({
  selector: 'mca-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss']
})
export class SalesComponent implements OnInit {

  dailySale: Product[];
  weeklySale: Product[]

  @Input('on-sale')
  set onSale(val: Product[]) {
    this.dailySale = val.filter(p => p.sale.type == 'daily');
    this.weeklySale = val.filter(p => p.sale.type == 'weekly');
  }

  constructor() { }

  ngOnInit(): void {
  }

}
