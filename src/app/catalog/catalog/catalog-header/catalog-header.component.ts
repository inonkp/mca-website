import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mca-catalog-header',
  templateUrl: './catalog-header.component.html',
  styleUrls: ['./catalog-header.component.scss']
})
export class CatalogHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
