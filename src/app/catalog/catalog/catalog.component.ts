import {AfterViewInit, Component, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {MCAImage} from 'src/app/shared/entities/mca-image';
import {Product} from 'src/app/shared/entities/product';
import { Location } from '@angular/common';
import { faAngleDown } from "@fortawesome/free-solid-svg-icons/faAngleDown";
import {QueryParams} from '@ngrx/data';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductsService} from '../services/products.service';
import { take } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { McaSeoService } from 'src/app/shared/services/mca-seo.service';

@Component({
  selector: 'mca-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent implements OnInit, OnDestroy, AfterViewInit {
  static readonly PAGE_SIZE = 12;
  static readonly ROW_SIZE = 4;
  static readonly SELECT_QUERY = "title,catalog_img,mbl_catalog_image,slug,tags,rating,rating_num";

  get faAngleDown(){
    return faAngleDown;
  }

  get rowSize(){
    return CatalogComponent.ROW_SIZE;
  }

  loaded: boolean;
  currentCatalogSize: number;

  get queryParams(): QueryParams {
    return this.mAllParams;
  }

  @Output('load')
  loadEmitter = new EventEmitter<void>();

  mPartialParams: QueryParams;
  mAllParams: QueryParams;

  products: Array<Product>[];
  tags: string[] = [];
  header: string;
  start: number;
  limit: number;
  isLoading = false;
  allElementsLoaded = false;
  subscription : Subscription;

  @HostListener('window:scroll', [])
  onScroll(): void {
    if ((window.innerHeight + window.scrollY) + 1>= document.body.scrollHeight) {
      this.showMoreProducts();
    }
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private productService: ProductsService,
    private seo: McaSeoService,
    private readonly location: Location
  ) {
    let data = activatedRoute.snapshot.data

    /**
     * Getting the partial params for future queries.
     * should remove all this in favor of pagination.
     */
    let paramMap = activatedRoute.snapshot.paramMap;
    let param = paramMap.get(data.catalogParam)
    this.applySeo(param, activatedRoute.snapshot.data.title(param));

    this.mPartialParams = data.query(param);

    this.init(data.products.length ?? 0);
    if(data.products.length > 0){
      for(let i = 0; i<data.products.length; i+=CatalogComponent.PAGE_SIZE){
        this.products.push(data.products.slice(i, i+CatalogComponent.PAGE_SIZE));
      }

    }

  }


  async ngOnInit(): Promise<void> {
  }

  ngAfterViewInit(): void {
    let slug = this.activatedRoute.snapshot.queryParamMap.get('product_view');

    if(slug){
      setTimeout(() => {
        document.getElementById('product-card-' + slug)?.scrollIntoView({
          behavior: 'smooth',
          block: 'center'
        });
      },500)

    }

  }

  applySeo(param: string, title: string){
    this.seo.index()
    this.seo.setTitle(title + " Catalog")
    let seoDescription = this.activatedRoute.snapshot.data?.seo?.(param);
    if(seoDescription){
      this.seo.setDescription(seoDescription);
    }else{
      this.seo.setDescription("Multi Canvas Art " + title + " Catalog");
    }
    this.seo.setCatalogLd(title);
  }

  updateQueryParmas(additionalQuery: QueryParams){
    this.mAllParams = {
      ... this.productService.getProductsQuery(this.start, this.limit),
      ... additionalQuery
    }
    this.mAllParams.select = CatalogComponent.SELECT_QUERY;
  }

  async pullProducts(params: QueryParams): Promise<void> {
    this.isLoading = true;
    const products: Product[] = await this.productService.getProducts(params);
    this.products.push(products);
    this.currentCatalogSize+= products.length;
    this.isLoading = false;
    if (!products.length) {
      this.allElementsLoaded = true;
    }

    // this keeps the context for future back presses
    var searchParams = new URLSearchParams(window.location.search);
    searchParams.set("total_page_num", Math.ceil(this.currentCatalogSize / CatalogComponent.PAGE_SIZE).toString());
    this.location.replaceState(this.router.url.split("?")[0], searchParams.toString());
  }

  public showMoreProducts(): void {
    if (this.allElementsLoaded || this.isLoading) {
      return;
    }

    this.start = this.currentCatalogSize ;
    this.updateQueryParmas(this.mPartialParams);
    this.pullProducts(this.queryParams);
  }

  public init(initialCatalogSize: number): void {
    this.products = [];
    this.start = 0;
    this.limit = CatalogComponent.PAGE_SIZE;
    this.currentCatalogSize = initialCatalogSize;
  }

  ngOnDestroy(): void {

  }
}
