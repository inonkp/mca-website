import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/shared/entities/product';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'mca-catalog-page',
  templateUrl: './catalog-page.component.html',
  styleUrls: ['./catalog-page.component.scss']
})
export class CatalogPageComponent implements OnInit {
  tags: string[];
  mProducts: Product[];

  @Input('page-num')
  pageNum: number;

  @Input("row-size")
  rowSize: number;

  @Input('products')
  set products(val: Product[]){
    this.mProducts = val;
    this.mProducts.forEach(async product => {
      this.tags = [...new Set(this.tags.concat(product.tags))];
    });
  }

  get size(){
    return this.mProducts.length;
  }

  get products(){
    return this.mProducts;
  }

  constructor(private readonly location: Location, private router: Router) {
    this.tags = [];
   }

  ngOnInit(): void {
  }

  getProductsByRow(row: number){
    return this.products.slice((row-1) * this.rowSize, row * this.rowSize);
  }

  onProductRoute(slug: string){
    var searchParams = new URLSearchParams(window.location.search);
    searchParams.set("product_view", slug);
    this.location.replaceState(this.router.url.split("?")[0], searchParams.toString());
  }

}
