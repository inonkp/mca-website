import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'mca-no-results',
  templateUrl: './no-results.component.html',
  styleUrls: ['./no-results.component.scss']
})
export class NoResultsComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  returnToHomePage(){
    this.router.navigate(["/"]);
  }

}
