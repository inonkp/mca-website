import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CateogriesFeatureComponent } from './categories-feature/categories-feature.component';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import { ProductCardComponent } from './product-card/product-card.component';
import { CategoryFeatureComponent } from './categories-feature/category-feature/category-feature.component';
import { ProductViewComponent } from './product-view/product-view.component';
import { SharedModule } from '../shared/shared.module';
import { FrontPageComponent } from './front-page/front-page.component';
import { CatalogComponent } from './catalog/catalog.component';
import { ScrollingModule } from '@angular/cdk/scrolling';

import { FeedbackModule } from '../feedback/feedback.module';
import { ProductOptionsComponent } from './product-view/product-options/product-options.component';
import { GalleryImagesComponent } from './product-view/gallery-images/gallery-images.component';
import { CatalogPageComponent } from './catalog/catalog-page/catalog-page.component';
import { ProductRowComponent } from './product-row/product-row.component';
import { RelatedProductsComponent } from './product-view/related-products/related-products.component';
import { CatalogHeaderComponent } from './catalog/catalog-header/catalog-header.component';
import { CatalogRoutingModule } from './catalog-routing.module';
import { ProductTagsComponent } from './product-view/product-tags/product-tags.component';
import { CartButtonComponent } from './product-view/cart-button/cart-button.component';
import { CheckoutModule } from '../checkout/checkout.module';
import { NoResultsComponent } from './catalog/no-results/no-results.component';
import { ProductRatingComponent } from './product-card/product-rating/product-rating.component';
import { FiveStarComponent } from './front-page/five-star/five-star.component';
import { ProductCardHeaderComponent } from './product-card/product-card-header/product-card-header.component';
import { MultiProductComponent } from './sales/multi-product/multi-product.component';
import { TopRatedComponent } from './front-page/top-rated/top-rated.component';
import { SalesComponent } from './front-page/sales/sales.component';
import { ProductDetailsComponent } from './product-view/product-details/product-details.component';
import { RowLayoutComponent } from './product-view/row-layout/row-layout.component';
import { ColumnLayoutComponent } from './product-view/row-layout/column-layout/column-layout.component';
import { ProductFormComponent } from './product-form/product-form.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MainImageComponent } from './product-view/gallery-images/main-image/main-image.component';

@NgModule({
  declarations: [
    CateogriesFeatureComponent,
    ProductCardComponent,
    CategoryFeatureComponent,
    ProductViewComponent,
    FrontPageComponent,
    CatalogComponent,
    ProductOptionsComponent,
    GalleryImagesComponent,
    CatalogPageComponent,
    ProductRowComponent,
    RelatedProductsComponent,
    CatalogHeaderComponent,
    ProductTagsComponent,
    CartButtonComponent,
    NoResultsComponent,
    ProductRatingComponent,
    FiveStarComponent,
    ProductCardHeaderComponent,
    MultiProductComponent,
    TopRatedComponent,
    SalesComponent,
    ProductDetailsComponent,
    RowLayoutComponent,
    ColumnLayoutComponent,
    ProductFormComponent,
    MainImageComponent
  ],
  imports: [
    MatSelectModule,
    CatalogRoutingModule,
    MatProgressSpinnerModule,
    CommonModule,
    SharedModule,
    CheckoutModule,
    MatIconModule,
    ScrollingModule,
    FeedbackModule
  ],
  exports: [],
  providers: []
})
export class CatalogModule {
}
