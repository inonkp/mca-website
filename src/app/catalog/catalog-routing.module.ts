import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from  '@angular/router';
import { ProductViewComponent } from './product-view/product-view.component';
import {default as categoriesModel} from "src/assets/data/categories.json";
import { CatalogComponent } from './catalog/catalog.component';
import { ProductsResolverService } from './services/products-resolver.service';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';
import { NoResultsComponent } from './catalog/no-results/no-results.component';

const routes: Routes = [
  {
    path: 'categories',
    data: {breadcrumb: 'Category'},
    children: [
      {
        path:'',
        component: NoResultsComponent,
        pathMatch: 'full'
      },
      {
      path: ':name',
      resolve: {products: ProductsResolverService},
      data: {
        breadcrumb: (slug: string) => {
          let catModel = categoriesModel.find(cat => cat.slug == slug);
          return catModel?.name ?? slug.replace("-", " ").replace(/\b\w/g, l => l.toUpperCase());
        },
        title: (slug: string) => {
          let catModel = categoriesModel.find(cat => cat.slug == slug);
          return catModel?.name ?? slug.replace("-", " ").replace(/\b\w/g, l => l.toUpperCase());
        },
        seo: (slug) => {
          let catModel = categoriesModel.find(cat => cat.slug == slug);
          return catModel?.description;
        },
        catalogParam: "name",
        query: (category) => {
          return {
            filter: JSON.stringify({categories: {$elemMatch:  {$eq: category}}})
          }
        }
      },
      component: CatalogComponent
    }]
  },
  {
    path: 'tags',
    data: {breadcrumb: 'Tag'},
    children: [
      {
        path:'',
        component: NoResultsComponent,
        pathMatch: 'full'
      },
      {
        path: ':name',
        resolve: {products: ProductsResolverService},
        data: {
          catalogParam: "name",
          breadcrumb: (tag) => tag,
          query: (tag) => {
            return {
              filter: JSON.stringify({tags: {$elemMatch:  {$eq: tag}}})
            };
          },
          title: (tag: string) => tag
        },
        component: CatalogComponent
      }]
  },
  {
    path: 'search',
    data: {breadcrumb: 'Search'},
    children: [
      {
        path:'',
        component: NoResultsComponent,
        pathMatch: 'full'
      },
      {
      path: ':query',
      resolve: {products: ProductsResolverService},
      data: {
        breadcrumb: (q) => q,
        catalogParam: "query",
        query: (search) => {
          let orQuery = {
            $or: [
              {tags: {$elemMatch:  {$eq: search}}},
              {title : {$regex : `(?i).*${search}.*`}}
            ]
          }
          return {
            filter: JSON.stringify(orQuery)
          }
        },
        title: (q) => q
      },
      component: CatalogComponent
    }]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes),
    ],
  exports: [RouterModule]
})
export class CatalogRoutingModule { }
