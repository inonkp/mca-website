import { Injectable } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';

@Injectable({
  providedIn: 'root'
})
export class MediaService {

  get lg(): boolean{
    return this.media.isActive("gt-sm");
  }

  get observer() {
    return this.media;
  }

  constructor(private media: MediaObserver) { }
}
