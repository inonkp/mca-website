import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { EntityCollectionService, EntityServices } from '@ngrx/data';
import { ModelItem } from '../entities/model-item';

@Injectable({
  providedIn: 'root'
})
export class ModelService implements Resolve<ModelItem[]> {

  get model() {
    return this.productsModel;
  }

  private productsModel: ModelItem[];
  private modelService: EntityCollectionService<ModelItem>;

  constructor(entityServices: EntityServices) {
    this.modelService = entityServices.getEntityCollectionService('Model');
  }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if(this.productsModel) {
      return this.productsModel;
    }
    // this.productsModel = [];
    this.productsModel = await this.modelService.load({

      }).toPromise();

    return this.productsModel;
  }

  private copyModel(model: ModelItem): ModelItem {
    return JSON.parse(JSON.stringify(model));
  }

  getModelItem(partialModel: Partial<ModelItem>): ModelItem{
    let model = this.productsModel.find(val => val.framing == partialModel.framing
      && val.size == partialModel.size && val.template == partialModel.template)

    if(!model) {
      return null;
    }
    return this.copyModel(model);
  }

  getModelItemById(id: string) {
    let model = this.productsModel.find(val => val.id == id);

    if(!model) {
      return null;
    }
    return this.copyModel(model);
  }
}
