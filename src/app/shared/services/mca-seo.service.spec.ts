import { TestBed } from '@angular/core/testing';

import { McaSeoService } from './mca-seo.service';

describe('McaSeoService', () => {
  let service: McaSeoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(McaSeoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
