import { Injectable } from '@angular/core';
import { JsonLdService, NgxSeoMetaTagAttr, SeoSocialShareService } from 'ngx-seo';
import { Product } from '../entities/product';

@Injectable({
  providedIn: 'root'
})
export class McaSeoService {
  readonly SITE_TITLE = "Multi Canvas Art";
  constructor(private ngxSeoService: SeoSocialShareService, private jsonLdService: JsonLdService) {

  }

  setTitle(title: string){
    this.ngxSeoService.setTitle(title + " | " + this.SITE_TITLE);
  }

  setDescription(description: string){
    this.ngxSeoService.setDescription(description);
  }

  formatAlt(alt: string){
    return alt + ' Multi Canvas Art';
  }

  setAboutDescription(){
    this.setDescription('Multi Canvas Art is here to bring you wall art of your favorite tv shows and movies. Since 2017, Multi Canvas Art has provided premium canvas wall art worldwide.')
  }

  setProductJsonLd(product: Product){
    const json = {
      'brand': this.SITE_TITLE,
      'category': product.title + ' Canvas Art',
      'itemCondition': "NEW"
    };
    if(product.rating){
      json['aggregateRating'] = product.rating.toFixed(1);
    }

    this.jsonLdService.setData(this.jsonLdService.getObject('Product', json));
  }

  setCatalogLd(param: string){
    const json = {
      'brand': this.SITE_TITLE,
      'category': (param + ' Canvas Art').trim(),
      'itemCondition': "NEW",
      'variesBy' : 'Model'
    };
    this.jsonLdService.setData(this.jsonLdService.getObject('ProductGroup', json));
  }

  noindex(){
    this.ngxSeoService.setMetaTag({
      attr: NgxSeoMetaTagAttr.name,
      attrValue: "robots",
      value: "noindex, noarchive"
    });


  }


  index(){
    this.ngxSeoService.setMetaTag({
      attr: NgxSeoMetaTagAttr.name,
      attrValue: "robots",
      value: "index, archive, follow"
    });
  }
}
