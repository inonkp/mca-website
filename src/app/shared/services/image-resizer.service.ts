import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { HttpUrlGenerator } from '@ngrx/data';
import { take } from 'rxjs/operators';
import { FullImageDialogComponent } from '../components/full-image-dialog/full-image-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class ImageResizerService {
  endpoint: string;
  constructor(private dialog: MatDialog, private httpUrlGenerator: HttpUrlGenerator) {
    this.endpoint = httpUrlGenerator.collectionResource("MCAImage", "");
  }

  getResized(imgUrl: string, width: number, height: number){
    return this.endpoint + `?resize=${width}x${height}&path=` + new URL(imgUrl).pathname.substr(1);
  }

  public openLargeImgModal(img: string): void {

    const dialogRef = this.dialog.open(FullImageDialogComponent, {
      data: {

        dialogImg: img,

      },
      maxWidth: '75vw',
      minWidth: '75vw',
      maxHeight: '80vh',
      minHeight: '95vh',
      panelClass: ['full-screen-modal']
    });

    dialogRef.afterClosed().pipe(take(1)).subscribe(res => {});
  }

}
