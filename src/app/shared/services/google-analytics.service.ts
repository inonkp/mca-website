import { Injectable } from '@angular/core';

declare const ga: {
  (...args: any[]): () => void;
};

@Injectable({
  providedIn: 'root'
})
export class GoogleAnalyticsService {

  constructor() { }

  logPageView(url: string) {
    if(typeof ga === "undefined" || !ga){
      return;
    }
    ga("set", "page", url);
    ga("send", "pageview");
  }
}
