import {Component, OnInit, Inject, ChangeDetectorRef} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Router, Event as RouterEvent, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router'

@Component({
  selector: 'mca-full-image-dialog',
  templateUrl: './full-image-dialog.component.html',
  styleUrls: ['./full-image-dialog.component.scss']
})
export class FullImageDialogComponent implements OnInit {
  index = 0;
  isLoading = true;

  constructor(
    public dialogRef: MatDialogRef<FullImageDialogComponent>,
    private changeDetectorRef: ChangeDetectorRef,
    @Inject(MAT_DIALOG_DATA) public imageData: any,
  ) {}

  ngOnInit(): void {}

  imgLoaded(): void {
    this.isLoading = false;
  }
}
