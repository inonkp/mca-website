import { Component, Input, OnInit } from '@angular/core';
import { Price } from '../../entities/price';

@Component({
  selector: 'mca-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.scss']
})
export class PriceComponent implements OnInit {

  @Input('price')
  set price(val: Price | null) {
    if(!val) {
      return;
    }
    
    this.total = val?.total;
    this.subtotal = val?.subtotal;
    this.description = val.description;
  }

  @Input('label')
  label: string;

  total: string;
  subtotal: string;
  description: string;

  @Input('disabled')
  disabled: boolean;

  @Input('font-size')
  fontSize: string;

  constructor() {
    this.disabled = false;
    this.label = '';
    this.fontSize = 'large';
   }

  ngOnInit(): void {

  }

  hasDiscount() {
    return this.subtotal != this.total;
  }

}
