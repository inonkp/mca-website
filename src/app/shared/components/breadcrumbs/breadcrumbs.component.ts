import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, UrlSegment } from '@angular/router';
import { take } from 'rxjs/operators';

@Component({
  selector: 'mca-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent implements OnInit {

  breadcrumbs: string[];
  constructor(private activeatedRoute: ActivatedRoute) {
    this.breadcrumbs = [];
  }

  ngOnInit(): void {
    this.initCrumbs();
  }

  async initCrumbs(){
    let routes = this.activeatedRoute.pathFromRoot;
    routes.forEach(async route => {
      let segment = await route.url.pipe(take(1)).toPromise();
      let data =  await route.data.pipe(take(1)).toPromise();
      let path = segment[0].path;
      let breadcrumb = data.breadcrumb ?? false;
      if(breadcrumb instanceof Function){
        breadcrumb = breadcrumb(path);
      }
      if(breadcrumb == ":"){
        breadcrumb = path;
      }

      if(breadcrumb){
        this.breadcrumbs.push(breadcrumb);
      }
    });
  }

}
