import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cdn'
})
export class CdnPipe implements PipeTransform {

  readonly s3 = 'mca-ng.s3.amazonaws.com';
  readonly cdn = 'd37ytqeyzc61j3.cloudfront.net';
  transform(value: string, ...args: unknown[]): string {
    return value.replace(this.s3, this.cdn);
  }

}
