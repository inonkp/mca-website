import { ComponentFactoryResolver, Directive, EmbeddedViewRef, HostBinding, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { LoadingAnimationComponent } from './loading-animation.component';

@Directive({
  selector: '[mcaLoadAnimation]'
})
export class LoadAnimationDirective {
  // @HostBinding('style.display')
  // backgroundColor:string = 'none';

  @Input() set mcaLoadAnimation(finAnimate: boolean){
    this.viewContainer.clear();
    if(finAnimate){
      this.viewContainer.insert(this.view);
    }else{
      this.init();
    }
  }

  onViewDestroyed(){
    this.viewContainer.detach();
  }
  private view: EmbeddedViewRef<any>;
  constructor(private resolver: ComponentFactoryResolver, private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef) {

     }

     init(){
      this.view = this.templateRef.createEmbeddedView(null);
      this.view.detectChanges();
      this.view.onDestroy(this.onViewDestroyed.bind(this));
      let factory = this.resolver.resolveComponentFactory(LoadingAnimationComponent);
      // viewContainer.createEmbeddedView(templateRef)
      const componentRef = this.viewContainer.createComponent<LoadingAnimationComponent>(factory);
      // this.viewContainer.createComponent(factory);
     }

}
