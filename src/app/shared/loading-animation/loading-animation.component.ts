import { Component, OnInit } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { MediaService } from '../services/media.service';

@Component({
  selector: 'mca-loading-animation',
  templateUrl: './loading-animation.component.html',
  styleUrls: ['./loading-animation.component.scss']
})
export class LoadingAnimationComponent implements OnInit {

  get marginLeft(){
    return !this.media.lg ? "25%" : "40%";
  }
  constructor(private media: MediaService) { }

  ngOnInit(): void {
  }

}
