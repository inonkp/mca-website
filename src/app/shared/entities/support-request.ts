export interface SupportRequest {
  name: String,
  email: string,
  order_id? : string;
  subject: string;
  body: string;
  'short-id': string;
}
