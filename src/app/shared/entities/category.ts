export interface Category {
  name: string;
  slug: string;
  weight: number;
}
