export interface Price {
    total: string;
    subtotal: string;
    description?: string;
}