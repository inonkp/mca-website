import { CartItem } from "./cart-item";
import { Coupon } from "./copoun";
import { Price } from "./price";
import { ShippingItem } from "./shipping-item";

type OrderStatus = "pending-payment" | "processing" | "complete";

export interface MCAOrder{
  id?: string;
  price: Price;
  coupon: string;
  payment_method?: string;
  payment_details: any;
  shipping: ShippingItem;
  address: any;
  cart: Partial<CartItem>[];
  cart_cost: Price;
  status: OrderStatus;
  'short-id'?: string;
}
