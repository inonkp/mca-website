import { Sale } from "./sale";

export interface Product {
  id: string;
  title: string;
  slug: string;
  SKU?: string;
  templates: string[];
  image?: string;
  catalog_img: string;
  mbl_catalog_image?: string;
  catalog_imgs?: CatalogImgs;
  tags?: string[];
  description: string;
  images_json?: any;
  categories?: string[];
  rating?: number;
  rating_num?: number;
  likes? : number;
  sale?: Sale;
}

export interface CatalogImgs {
  H1: string;
  H3: string;
  H4: string;
  H5: string;
}
