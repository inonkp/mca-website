export enum CommentStatus{
  APPROVED = "approved",
  PENDING = "pending"
}

export interface MCAComment {
  comment: string;
  createdAt: string;
  id: string;
  images?: any[];
  product_id: string;
  published_at: string;
  stars: number;
  status: CommentStatus;
  name: string;
}
