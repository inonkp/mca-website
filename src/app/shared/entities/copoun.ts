import { CartItem } from "./cart-item";
import { ShippingItem } from "./shipping-item";

export type Discount = {
  price: string;
  label: string;
}


export interface Coupon {
  valid: boolean;
  cartItems?: Partial<CartItem>[];
  shippingItems?: Partial<ShippingItem>[];
  code: string;
  itemsLabel?: string;
}
