export type Framing = "F" | "PO";
export type Size = "L" | "M" | "S";
export type Template =  "H5" | "H4" | "H3" | "H1";

export interface ModelItem{
  size: Size;
  template: Template;
  framing: Framing;
  price: string;
  description: string;
  weight: number;
  summary: string;
  id: string;
}
