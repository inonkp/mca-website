import { ProductOption } from "src/app/catalog/product-view/product-options/product-option";
import { Discount } from "./copoun";
import { Price } from "./price";
import { Product } from "./product";

export enum CartItemStatus{
  PENDING = "pending",
  COMPLETE = "complete"
}

export interface CartItem {
  id?: string;
  modelId: string;
  quantity: number;
  product: Partial<Product>;
  status: CartItemStatus;
  order_id?: string;
  price: Price | null;
}
