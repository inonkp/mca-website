export interface Address{
  "first-name": string;
  "last-name": string;
  phone: string;
  email: string;
  address1: string;
  address2: string;
  city: string;
  state: string;
  zipcode: string;
  country_code: string;
}
