import { Discount } from "./copoun";
import { Price } from "./price";


export interface ShippingItem{
  price: Price;
  shipping_method: string;
  shipping_method_description: string;
  tracking_number?: string;
  country_code: string;
  discount: Discount;
  id: string;
}
