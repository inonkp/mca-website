import { Product } from "./product";

export interface Sale {
    type: 'weekly' | 'daily';
    status: 'active' | 'pending' | 'expired';
    products: Product[];
    discount: string;
    category: string;
}