import { animate, AnimationBuilder, style } from '@angular/animations';
import { Directive, ElementRef, EventEmitter, HostListener, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[mcaFadeInLoad]',
  exportAs: 'mcaFadeInLoad'
})
export class FadeInLoadDirective implements OnInit {

  @Input('mcaFadeInLoad')
  set src(val: string) {

    this.loadEmitter.next(true)
  }

  loadEmitter = new EventEmitter<boolean>(true);

  @HostListener('load')
  onLoad(){
    const myAnimation = this.builder.build([
      style({ opacity: 0 }),
      animate(500, style({ opacity: 1 }))
    ]);

    if(this.el.nativeElement) {
      const player = myAnimation.create(this.el.nativeElement);
      player.play();
    }

    this.loadEmitter.next(false)
  }

  constructor(private el: ElementRef, private builder: AnimationBuilder) {

  }
  
  ngOnInit() {

  }

}
