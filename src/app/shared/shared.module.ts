import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { BREAKPOINT, DEFAULT_CONFIG, FlexLayoutModule, LayoutConfigOptions, LAYOUT_CONFIG } from '@angular/flex-layout';
import { MatPaginatorModule } from '@angular/material/paginator';
import { LoadingAnimationComponent } from './loading-animation/loading-animation.component';
import { LoadAnimationDirective } from './loading-animation/load-animation.directive';
import { BrowserModule } from '@angular/platform-browser';
import {ReactiveFormsModule} from '@angular/forms';
import { FadeInLoadDirective } from './fade-in-load/fade-in-load.directive';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpInterceptorService } from "./services/http-interceptor.service"
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { MatButtonModule } from '@angular/material/button';
import { MatBadgeModule } from "@angular/material/badge"
import { MatDialogModule } from '@angular/material/dialog';
import { FullImageDialogComponent } from './components/full-image-dialog/full-image-dialog.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { CdnPipe } from './cdn.pipe';
import { JsonLdModule } from 'ngx-seo';
import { PriceComponent } from './components/price/price.component';
import { DeviceDetectorService } from 'ngx-device-detector';

@NgModule({
  declarations: [
    LoadingAnimationComponent,
    LoadAnimationDirective,
    FadeInLoadDirective,
    BreadcrumbsComponent,
    FullImageDialogComponent,
    CdnPipe,
    PriceComponent
  ],
  imports: [
    MatCardModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    FontAwesomeModule,
    FlexLayoutModule,
    MatPaginatorModule,
    CommonModule,
    BrowserModule,
    MatBadgeModule,
    MatDialogModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    JsonLdModule,
  ],
  exports: [
    LoadingAnimationComponent,
    LoadAnimationDirective,
    MatCardModule,
    FontAwesomeModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FadeInLoadDirective,
    BreadcrumbsComponent,
    MatBadgeModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    CdnPipe,
    JsonLdModule,
    PriceComponent
  ],
  providers: [
    {
      provide: LAYOUT_CONFIG, useFactory:(devDetector: DeviceDetectorService) => {
        return {...DEFAULT_CONFIG, ssrObserveBreakpoints: []}
      }, deps: [DeviceDetectorService],
    },
    {provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true}
  ]
})
export class SharedModule { }
