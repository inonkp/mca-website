import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import { Coupon } from 'src/app/shared/entities/copoun';

import {
  EntityDataModule,
  HttpUrlGenerator,
  EntityHttpResourceUrls,
  DefaultDataServiceConfig,
  DefaultDataServiceFactory,
  EntityDataService,
  EntityCache,
  INITIAL_ENTITY_CACHE_STATE,
  EntityServices,
  EntityMetadataMap,
  EntityCollection,
  EntityAction
} from '@ngrx/data';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Action, ActionReducer, StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {CatalogModule} from './catalog/catalog.module';
import {HttpClientModule} from '@angular/common/http';
import {SharedModule} from './shared/shared.module';
import {HeaderModuleModule} from './header/header-module.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { environment } from 'src/environments/environment';
import { AboutComponent } from './about/about.component';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

const entityMetadata: EntityMetadataMap = {
  Model: { entityName: 'Model' },
  Product: {entityName: 'Product'},
  Sale: {entityName: 'Sale'},
  MCAImage: {entityName: 'MCAImage'},
  MCAComment: {entityName: 'MCAComment'},
  MCAOrder: {entityName: "MCAOrder"},
  CartItem: {entityName: 'CartItem', additionalCollectionState: {
    price: {
      total: 0,
      subtotal: 0
    },
    weight: 0
  }},
  ShippingItem: {entityName: "ShippingItem"},
  SupportRequest: {entityName: "SupportRequest"},
  DiscountCode: {entityName: "DiscountCode", selectId: (code: Coupon) => code.code}
};

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserAnimationsModule,
    HttpClientModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    EntityDataModule.forRoot({ entityMetadata: entityMetadata }),
    CatalogModule,
    AppRoutingModule,
    SharedModule,
    HeaderModuleModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  static mca = 'http://18.214.174.28:1337/';
  static aws = 'https://1qv1v9co5a.execute-api.us-east-1.amazonaws.com/default/';
  constructor(httpUrlGenerator: HttpUrlGenerator) {
    httpUrlGenerator.registerHttpResourceUrls(
      {
        Model: {
          entityResourceUrl: environment.api + 'model/',
          collectionResourceUrl: environment.api + 'model/'
        }
      }
    );
    
    httpUrlGenerator.registerHttpResourceUrls(
      {
        Product: {
          entityResourceUrl: environment.api + 'products/',
          collectionResourceUrl: environment.api + 'products/'
        }
      }
    );

    httpUrlGenerator.registerHttpResourceUrls(
      {
        Sales: {
          entityResourceUrl: environment.api + 'sales/',
          collectionResourceUrl: environment.api + 'sales/'
        }
      }
    );

    httpUrlGenerator.registerHttpResourceUrls(
      {
        MCAImage: {
          entityResourceUrl: AppModule.aws + 'images/',
          collectionResourceUrl: AppModule.aws + 'images/'
        }
      }
    );

    httpUrlGenerator.registerHttpResourceUrls(
      {
        MCAComment: {
          entityResourceUrl: environment.api + 'comments/',
          collectionResourceUrl: environment.api + 'comments/'
        }
      }
    );

    httpUrlGenerator.registerHttpResourceUrls(
      {
        CartItem: {
          entityResourceUrl: environment.api + 'cart-items/',
          collectionResourceUrl: environment.api + 'cart-items/'
        }
      }
    );

    httpUrlGenerator.registerHttpResourceUrls(
      {
        MCAOrder: {
          entityResourceUrl: environment.api + 'orders/',
          collectionResourceUrl: environment.api + 'orders/'
        }
      }
    );

    httpUrlGenerator.registerHttpResourceUrls(
      {
        ShippingItem: {
          entityResourceUrl: environment.api + 'shipping/',
          collectionResourceUrl: environment.api + 'shipping/'
        }
      }
    );

    httpUrlGenerator.registerHttpResourceUrls(
      {
        SupportRequest: {
          entityResourceUrl: environment.api + 'support/',
          collectionResourceUrl: environment.api + 'support/'
        }
      }
    );

    httpUrlGenerator.registerHttpResourceUrls(
      {
        DiscountCode: {
          entityResourceUrl: environment.api + 'discount-code/',
          collectionResourceUrl: environment.api + 'discount-code/'
        }
      }
    );
  }
}
