import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductViewComponent } from 'src/app/catalog/product-view/product-view.component';
import { AboutComponent } from './about/about.component';
import { FrontPageComponent } from './catalog/front-page/front-page.component';
import { FrontPageResolverService } from './catalog/services/front-page-resolver.service';
import { ProductResolverService } from './catalog/services/product-resolver.service';
import { CartComponent } from './checkout/cart/cart.component';
import { CheckoutComponent } from './checkout/checkout/checkout.component';
import { OrderComponent } from './checkout/order/order.component';
import { CartResolverService } from './checkout/services/cart-resolver.service';
import { CheckoutResolverService } from './checkout/services/checkout-resolver.service';
import { OrderResolverService } from './checkout/services/order-resolver.service';
import { ContactPageComponent } from './feedback/contact-page/contact-page.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ModelService } from './shared/services/model.service';


const routes: Routes = [
  { path : '', resolve: {model: ModelService},children: [
      { path: '', component: FrontPageComponent, resolve: {products: FrontPageResolverService}},
      { path: 'product/:slug', component: ProductViewComponent, resolve: {product: ProductResolverService} },
      { path: 'products/:slug', redirectTo: 'product/:slug' },
      { path: 'cart', component: CartComponent, resolve: {cartItems: CartResolverService} },
      { path: 'checkout', component: CheckoutComponent, resolve: {data: CheckoutResolverService}},
      { path: 'order/:order-id', component: OrderComponent, resolve: {data: OrderResolverService}},
      { path: 'contact', component: ContactPageComponent},
      { path: 'about', component: AboutComponent},
      {
        path: 'admin',
        loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)
      }
    ]
  },
  { path: "**" , component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking',
    scrollPositionRestoration: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
