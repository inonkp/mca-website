import {Component, OnDestroy, OnInit} from '@angular/core';
import {EntityCollectionDataService, EntityDataService} from '@ngrx/data';
import {CartItem} from '../../shared/entities/cart-item';
import {Router} from '@angular/router';
import {CartService} from '../../checkout/services/cart.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from "rxjs";

@Component({
  selector: 'mca-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  cartService: EntityCollectionDataService<CartItem>;
  private destroy$: Subject<void> = new Subject<void>();

  constructor(private cart: CartService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.subscribeToCartStream();
  }


  private subscribeToCartStream(): void {

  }

  public navigateToCartView(): void {
    this.router.navigate(['cart']);
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
