import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { HeaderComponent } from './header/header.component';
import { SharedModule } from '../shared/shared.module';
import { SearchComponent } from './search/search.component';
import { CatalogModule } from '../catalog/catalog.module';
import { CheckoutModule } from '../checkout/checkout.module';
import { MobileHeaderComponent } from './mobile-header/mobile-header.component';
import { MobileFooterComponent } from './mobile-footer/mobile-footer.component';
import { RouterModule } from '@angular/router';
import { HeaderIconsComponent } from './header-icons/header-icons.component';

@NgModule({
  declarations: [HeaderComponent, SearchComponent, MobileHeaderComponent, MobileFooterComponent, HeaderIconsComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressSpinnerModule,
    CatalogModule,
    CheckoutModule
  ],
  exports: [HeaderComponent, MobileHeaderComponent, MobileFooterComponent]
})
export class HeaderModuleModule { }
