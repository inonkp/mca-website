import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons/faEnvelope'
import { faInfo } from '@fortawesome/free-solid-svg-icons/faInfo'
import { faUser } from '@fortawesome/free-solid-svg-icons/faUser'

@Component({
  selector: 'mca-header-icons',
  templateUrl: './header-icons.component.html',
  styleUrls: ['./header-icons.component.scss']
})
export class HeaderIconsComponent implements OnInit {

  get faUser() {
     return faUser;
  }

  get faEnvelope(){
    return faEnvelope;
  }

  get faInfo(){
    return faInfo;
  }

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  routeTo(path: string){
    this.router.navigate([path]);
  }

}
