import { Component, OnInit, OnDestroy } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { debounceTime, takeUntil, tap } from 'rxjs/operators';
import { ProductsService } from '../../catalog/services/products.service';
import { Subject } from 'rxjs';
import { Product } from '../../shared/entities/product';
import { Router } from '@angular/router';
import { faSearch } from "@fortawesome/free-solid-svg-icons/faSearch";

@Component({
  selector: 'mca-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit, OnDestroy {

  get faSearch(){
    return faSearch;
  }

  searchForm: UntypedFormGroup;
  isLoading: boolean;
  products: Product[];
  tags: string[] = [];
  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    private productsService: ProductsService,
    private router: Router
  ) {
    this.isLoading = false;
  }

  ngOnInit(): void {
    this.generateSearchForm();
    this.searchInputData();
  }

  async getAutoCompleteProducts(name): Promise<void> {
    const params = {
      ... this.productsService.getProductsQuery(0,5),
      ... this.productsService.getSearchQuery(name)
    };
    this.productsService.addSelectQuery(params, ['slug', 'title', 'catalog_img', 'mbl_catalog_image'])
    const selectedProducts = await this.productsService.getProducts(params);
    this.products = selectedProducts;
  }

  public generateSearchForm(): void {
    this.searchForm = new UntypedFormGroup({
      search: new UntypedFormControl('', [])
    });
  }

  searchAll(){
    let query = this.searchForm.get('search').value;
    if(!query) return;

    this.searchForm.get('search').reset();
    this.router.navigate(['search/' + query]);
  }

  public searchInputData(): void {
    this.searchForm
      .get('search')
      .valueChanges
      .pipe(
        takeUntil(this.destroy$),
        debounceTime(300),
        tap(() => this.isLoading = true),
      )
      .subscribe(val => {
        if (!val) {
          this.products = [];
        } else {
          this.getAutoCompleteProducts(val);
        }
        this.isLoading = false;
      });
  }

  public getProductView(productView): void {
    this.router.navigate(['product/' + productView.slug]);
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
