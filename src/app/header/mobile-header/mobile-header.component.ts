import { Component, OnInit } from '@angular/core';
import { faBars } from "@fortawesome/free-solid-svg-icons/faBars";

@Component({
  selector: 'mca-mobile-header',
  templateUrl: './mobile-header.component.html',
  styleUrls: ['./mobile-header.component.scss']
})
export class MobileHeaderComponent implements OnInit {

  get faBars(){
    return faBars;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
