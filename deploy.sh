#!/bin/bash

usage() { echo "Usage: $0 [-v version]" 1>&2; exit 1; }

while getopts "v:" o; do
    case "${o}" in
        v)
            v=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${v}" ]; then
    usage
fi

# must be absolute path for -d
DEPLOY_DIR="/home/admin/mca-deploy/$v"

if [ -d "$DEPLOY_DIR" ]; then
    echo "version $v exists."
    usage
fi

changes="mca $v changes"
echo "input changes in new lines, CTRL+D to finish."
while read change
do
    changes="${changes}\n${change}"
    # do processing with each domain
done

echo "deploying to $DEPLOY_DIR"

mkdir -p $DEPLOY_DIR
echo $changes > "$DEPLOY_DIR/README"

sh ./prerender.sh
