const awsServerlessExpress = require("aws-serverless-express");
// Get the built application from dist folder
const app = require("./dist/mca/serverless/main");
const awsServerlessExpressMiddleware = require("aws-serverless-express/middleware");
// Use eventContext middleware
app.server.use(awsServerlessExpressMiddleware.eventContext());
// Create server proxy
const serverProxy = awsServerlessExpress.createServer(app.server);
// Create and export the handler function
module.exports.handler = (event, context) => awsServerlessExpress.proxy(serverProxy, event, context);
