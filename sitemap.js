  const { SitemapStream, streamToPromise } = require( 'sitemap' )
  const { Readable } = require( 'stream' )
  const fs = require('fs');
  const readline = require('readline');

  createSiteMap();
  // An array with your links


  async function createSiteMap(){
    const links = await processRouteFile();

    // Create a stream to write to
    const stream = new SitemapStream( { hostname: 'https://multicanvasart.com' } )

    // Return a promise that resolves with your XML string
    return streamToPromise(Readable.from(links).pipe(stream)).then((data) =>
      data.toString()
    )
  }

  async function processRouteFile() {
    let links = [];
    const fileStream = fs.createReadStream('mca-routes.txt');

    const rl = readline.createInterface({
      input: fileStream,
      crlfDelay: Infinity
    });
    // Note: we use the crlfDelay option to recognize all instances of CR LF
    // ('\r\n') in input.txt as a single line break.

    for await (const line of rl) {
      // Each line in input.txt will be successively available here as `line`.
      let priority = 0.7;
      let changefreq = "monthly";
      if(line.startsWith("/product/")) {
        priority = 0.3;
      } else if (line.startsWith("/categories/")) {
        priority = 0.5
      }

      links.push({
        url: line,
        priority: priority,
        changefreq: changefreq
      });
    }
    return links;
  }
